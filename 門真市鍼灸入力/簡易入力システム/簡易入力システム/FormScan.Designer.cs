﻿namespace WindowsFormsApplication1
{
    partial class FormScan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelCount = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelSelection = new System.Windows.Forms.Label();
            this.textBox_billname = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonCCW = new System.Windows.Forms.Button();
            this.buttonCW = new System.Windows.Forms.Button();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.textbox_billdate = new System.Windows.Forms.TextBox();
            this.buttonScan = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.buttonSelectDir = new System.Windows.Forms.Button();
            this.textBoxDir = new System.Windows.Forms.TextBox();
            this.panel0 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelProg = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.pictureBox1 = new WindowsFormsApplication1.UserControlImage();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel0.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelCount
            // 
            this.labelCount.AutoSize = true;
            this.labelCount.Location = new System.Drawing.Point(4, 20);
            this.labelCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelCount.Name = "labelCount";
            this.labelCount.Size = new System.Drawing.Size(74, 15);
            this.labelCount.TabIndex = 0;
            this.labelCount.Text = "labelCount";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelSelection);
            this.panel1.Controls.Add(this.textBox_billname);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.textbox_billdate);
            this.panel1.Controls.Add(this.buttonScan);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.labelCount);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(481, 454);
            this.panel1.TabIndex = 0;
            // 
            // labelSelection
            // 
            this.labelSelection.AutoSize = true;
            this.labelSelection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labelSelection.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelSelection.ForeColor = System.Drawing.Color.White;
            this.labelSelection.Location = new System.Drawing.Point(16, 184);
            this.labelSelection.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSelection.Name = "labelSelection";
            this.labelSelection.Size = new System.Drawing.Size(169, 190);
            this.labelSelection.TabIndex = 21;
            this.labelSelection.Text = "0.labelSelection\r\n1.labelSelection\r\n2.labelSelection\r\n3.labelSelection\r\n4.labelSe" +
    "lection\r\n5.labelSelection\r\n6.labelSelection\r\n7.labelSelection\r\n8.labelSelection\r" +
    "\n9.labelSelection\r\n";
            // 
            // textBox_billname
            // 
            this.textBox_billname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBox_billname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBox_billname.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.textBox_billname.Location = new System.Drawing.Point(77, 156);
            this.textBox_billname.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_billname.Name = "textBox_billname";
            this.textBox_billname.Size = new System.Drawing.Size(387, 22);
            this.textBox_billname.TabIndex = 5;
            this.textBox_billname.TextChanged += new System.EventHandler(this.textBox_billname_TextChanged);
            this.textBox_billname.Enter += new System.EventHandler(this.textBox_Enter);
            this.textBox_billname.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonCCW);
            this.groupBox1.Controls.Add(this.buttonCW);
            this.groupBox1.Controls.Add(this.buttonPrev);
            this.groupBox1.Controls.Add(this.buttonNext);
            this.groupBox1.Location = new System.Drawing.Point(212, 8);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(253, 80);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "画像チェック";
            // 
            // buttonCCW
            // 
            this.buttonCCW.BackColor = System.Drawing.Color.White;
            this.buttonCCW.Image = global::WindowsFormsApplication1.Properties.Resources.ImageCounterclockwise;
            this.buttonCCW.Location = new System.Drawing.Point(100, 12);
            this.buttonCCW.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonCCW.Name = "buttonCCW";
            this.buttonCCW.Size = new System.Drawing.Size(67, 62);
            this.buttonCCW.TabIndex = 2;
            this.buttonCCW.TabStop = false;
            this.buttonCCW.UseVisualStyleBackColor = false;
            this.buttonCCW.Click += new System.EventHandler(this.buttonCCW_Click);
            // 
            // buttonCW
            // 
            this.buttonCW.BackColor = System.Drawing.Color.White;
            this.buttonCW.Image = global::WindowsFormsApplication1.Properties.Resources.ImageClockwise;
            this.buttonCW.Location = new System.Drawing.Point(175, 12);
            this.buttonCW.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonCW.Name = "buttonCW";
            this.buttonCW.Size = new System.Drawing.Size(67, 62);
            this.buttonCW.TabIndex = 3;
            this.buttonCW.TabStop = false;
            this.buttonCW.UseVisualStyleBackColor = false;
            this.buttonCW.Click += new System.EventHandler(this.buttonCW_Click);
            // 
            // buttonPrev
            // 
            this.buttonPrev.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.buttonPrev.Location = new System.Drawing.Point(12, 22);
            this.buttonPrev.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(40, 38);
            this.buttonPrev.TabIndex = 0;
            this.buttonPrev.TabStop = false;
            this.buttonPrev.Text = "＜";
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.buttonNext.Location = new System.Drawing.Point(52, 22);
            this.buttonNext.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(40, 38);
            this.buttonNext.TabIndex = 1;
            this.buttonNext.TabStop = false;
            this.buttonNext.Text = "＞";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // textbox_billdate
            // 
            this.textbox_billdate.Font = new System.Drawing.Font("ＭＳ ゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textbox_billdate.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textbox_billdate.Location = new System.Drawing.Point(149, 115);
            this.textbox_billdate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textbox_billdate.MaxLength = 4;
            this.textbox_billdate.Name = "textbox_billdate";
            this.textbox_billdate.Size = new System.Drawing.Size(120, 31);
            this.textbox_billdate.TabIndex = 3;
            this.textbox_billdate.TextChanged += new System.EventHandler(this.textbox_billdate_TextChanged);
            this.textbox_billdate.Enter += new System.EventHandler(this.textBoxDate_Enter);
            this.textbox_billdate.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // buttonScan
            // 
            this.buttonScan.Location = new System.Drawing.Point(344, 188);
            this.buttonScan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonScan.Name = "buttonScan";
            this.buttonScan.Size = new System.Drawing.Size(129, 50);
            this.buttonScan.TabIndex = 6;
            this.buttonScan.Text = "取り込み実行";
            this.buttonScan.UseVisualStyleBackColor = true;
            this.buttonScan.Click += new System.EventHandler(this.buttonScan_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 160);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "支払先";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 124);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "請求年月（和暦年）";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // buttonSelectDir
            // 
            this.buttonSelectDir.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSelectDir.Location = new System.Drawing.Point(0, 0);
            this.buttonSelectDir.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonSelectDir.Name = "buttonSelectDir";
            this.buttonSelectDir.Size = new System.Drawing.Size(100, 30);
            this.buttonSelectDir.TabIndex = 0;
            this.buttonSelectDir.Text = "対象フォルダ";
            this.buttonSelectDir.UseVisualStyleBackColor = true;
            this.buttonSelectDir.Click += new System.EventHandler(this.buttonSelectDir_Click);
            // 
            // textBoxDir
            // 
            this.textBoxDir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxDir.Location = new System.Drawing.Point(100, 0);
            this.textBoxDir.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxDir.Name = "textBoxDir";
            this.textBoxDir.ReadOnly = true;
            this.textBoxDir.Size = new System.Drawing.Size(907, 22);
            this.textBoxDir.TabIndex = 1;
            this.textBoxDir.TabStop = false;
            // 
            // panel0
            // 
            this.panel0.Controls.Add(this.textBoxDir);
            this.panel0.Controls.Add(this.buttonSelectDir);
            this.panel0.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel0.Location = new System.Drawing.Point(0, 0);
            this.panel0.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel0.Name = "panel0";
            this.panel0.Size = new System.Drawing.Size(1007, 30);
            this.panel0.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 30);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(481, 512);
            this.panel2.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.labelProg);
            this.panel3.Controls.Add(this.progressBar1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 454);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(481, 58);
            this.panel3.TabIndex = 1;
            // 
            // labelProg
            // 
            this.labelProg.AutoSize = true;
            this.labelProg.Location = new System.Drawing.Point(4, 14);
            this.labelProg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelProg.Name = "labelProg";
            this.labelProg.Size = new System.Drawing.Size(69, 15);
            this.labelProg.TabIndex = 0;
            this.labelProg.Text = " labelProg";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(87, 14);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(387, 29);
            this.progressBar1.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.AutoScroll = true;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Enabled = false;
            this.pictureBox1.Image = null;
            this.pictureBox1.ImageLocation = null;
            this.pictureBox1.Location = new System.Drawing.Point(481, 30);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(526, 512);
            this.pictureBox1.TabIndex = 0;
            // 
            // FormScan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1007, 542);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel0);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FormScan";
            this.Text = "スキャン実行";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormScan_FormClosing);
            this.Load += new System.EventHandler(this.FormScan_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.panel0.ResumeLayout(false);
            this.panel0.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelCount;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonCCW;
        private System.Windows.Forms.Button buttonCW;
        private System.Windows.Forms.Button buttonScan;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textbox_billdate;
        private System.Windows.Forms.Label label3;
        private UserControlImage pictureBox1;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrev;
        private System.Windows.Forms.Button buttonSelectDir;
        private System.Windows.Forms.TextBox textBoxDir;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel0;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelProg;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox textBox_billname;
        private System.Windows.Forms.Label labelSelection;
    }
}