﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                SQLClass.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Close();
            }

            // グループ一覧の更新 （全て入力済のものについてはチェックのしない。）
            UpdateList();
        }


        /// <summary> グループ名一覧を更新する </summary>
        void UpdateList()
        {
            int gid = -1;
            if (dataGridView1.SelectedRows.Count > 0) gid = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["gid"].Value);
            
            string query = "SELECT * FROM V_GROUP";
            if (checkBox1.Checked == false) query += " WHERE notoutput > 0";
            using (var dt = SQLClass.CreateDbDataTable(query))
            {
                dt.Columns.Add("chk", typeof(bool));
                foreach (DataRow row in dt.Rows) row["chk"] = (Convert.ToInt32(row["notoutput"]) > 0 &&
                                                               Convert.ToInt32(row["notinput"]) == 0 &&
                                                               Convert.ToInt32(row["notverify"]) == 0);
                dataGridView1.DataSource = dt;
            }

            foreach (DataGridViewColumn col in dataGridView1.Columns) col.ReadOnly = true;

            
            dataGridView1.Columns["chk"].ReadOnly = false;
            dataGridView1.Columns["chk"].HeaderText = "出";
            dataGridView1.Columns["chk"].DisplayIndex = 0;

            dataGridView1.Columns["gid"].HeaderText = "ｸﾞﾙｰﾌﾟ";

            dataGridView1.Columns["minfn"].HeaderText = "ﾌｧｲﾙ名";
            dataGridView1.Columns["minfn"].DefaultCellStyle.Format = "00000000～";

            dataGridView1.Columns["maxfn"].HeaderText = "";
            dataGridView1.Columns["maxfn"].DefaultCellStyle.Format = "00000000";

            dataGridView1.Columns["sdate"].HeaderText = "スキャン";
            dataGridView1.Columns["sdate"].DefaultCellStyle.Format = "0000-00-00";

            dataGridView1.Columns["billmonth"].HeaderText = "請求月";

            //20190613152410 furukawa st ////////////////////////
            //年表記改修
            
            dataGridView1.Columns["billmonth"].DefaultCellStyle.Format = "00/00";
                    //dataGridView1.Columns["billmonth"].DefaultCellStyle.Format = "平00年00月";
            //20190613152410 furukawa ed ////////////////////////

            dataGridView1.Columns["billname"].Visible = true;
            dataGridView1.Columns["billname"].HeaderText = "支払先";

            dataGridView1.Columns["cnt"].Visible = true;
            dataGridView1.Columns["cnt"].HeaderText = "全枚数";
            
            dataGridView1.Columns["notinput"].Visible = true;
            dataGridView1.Columns["notinput"].HeaderText = "入力未";

            dataGridView1.Columns["notverify"].Visible = true;
            dataGridView1.Columns["notverify"].HeaderText = "ﾁｪｯｸ未";

            dataGridView1.Columns["notoutput"].Visible = true;
            dataGridView1.Columns["notoutput"].HeaderText = "出力未";

            dataGridView1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
            dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);


            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                row.Cells["chk"].ReadOnly = (Convert.ToInt32(row.Cells["notinput"].Value) > 0 ||
                                             Convert.ToInt32(row.Cells["notverify"].Value) > 0);
            }



            dataGridView1.ClearSelection();

            if (gid >= 0)
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (Convert.ToInt32( row.Cells["gid"].Value)  == gid)
                    {
                        dataGridView1.CurrentCell = row.Cells["gid"];
                        dataGridView1.ClearSelection();
                        row.Selected = true;
                        break;
                    }
                }
            }


        }



        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                SQLClass.Close();
            }
            catch (Exception ex)
            {
            }
        }



        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            try 
	        {
                string col = dataGridView1.Columns[e.ColumnIndex].Name;
                if (col.IndexOf("not") == 0 && Convert.ToInt32(e.Value) > 0)
                {
                    e.CellStyle.BackColor = Color.Pink;
                    e.CellStyle.SelectionBackColor = Color.Red;
                }
                else
                {
                    e.CellStyle.BackColor = Color.White;
                    e.CellStyle.SelectionBackColor = Color.Blue;
                }

                if (e.RowIndex <= 0 || col == "chk" || col == "billname") 
                { 
                    e.CellStyle.ForeColor=Color.Black;
                    e.CellStyle.SelectionForeColor = Color.White;
                }
                else if (Convert.ToInt32(dataGridView1["gid", e.RowIndex].Value) != Convert.ToInt32(dataGridView1["gid", e.RowIndex - 1].Value))
                {
                    e.CellStyle.ForeColor = Color.Black;
                    e.CellStyle.SelectionForeColor = Color.White;
                }
                else
                {
                    e.CellStyle.ForeColor = e.CellStyle.BackColor;
                    e.CellStyle.SelectionForeColor = e.CellStyle.SelectionBackColor;
                }

	        }
	        catch (Exception)
	        {
	        }
        }

        /// <summary> スキャン </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonScan_Click(object sender, EventArgs e)
        {
            using (var f = new FormScan())
            {
                f.ShowDialog();
            }
            UpdateList();
        }

        /// <summary> 入力</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInout_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count <= 0) return;

            using (var f = new FormInput(Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["gid"].Value), false))
            {
                f.ShowDialog();
            }
            UpdateList();
        }

        // <summary> チェック</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonChecke_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count <= 0) return;

            using (var f = new FormInput(Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["gid"].Value), true))
            {
                f.ShowDialog();
            }
            UpdateList();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            UpdateList();
        }


        // データ出力
        private void buttonOutput_Click(object sender, EventArgs e)
        {   
            // 出力グループ一覧 （DataGridViewで選択したもの）
            var gList = new HashSet<int>();
            foreach (DataGridViewRow row in dataGridView1.Rows)
	        {
                if ((bool)row.Cells["chk"].Value)
                {
                    gList.Add(Convert.ToInt32(row.Cells["gid"].Value.ToString()));
                }
	        }
            if (gList.Count <= 0) 
            {
                MessageBox.Show("出力対象なし");
                return;
            }

            // 出力先のデータを選択 （件数も表示）
            folderBrowserDialog1.SelectedPath = System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            folderBrowserDialog1.Description = "出力先選択";
            if(folderBrowserDialog1.ShowDialog() != DialogResult.OK) return;

            // ROOTの取得 ： \柔整鍼灸出力\yyMMdd_####
            int rootId = 0;
            string rootDir;
            while (Directory.Exists(folderBrowserDialog1.SelectedPath + "\\柔整鍼灸出力\\" + DateTime.Now.ToString("yyMMdd") + "_" + rootId.ToString("0000"))) rootId++;
            rootDir = folderBrowserDialog1.SelectedPath + "\\柔整鍼灸出力\\" + DateTime.Now.ToString("yyMMdd") + "_" + rootId.ToString("0000");
            
            // 画像の出力
            var fList = new List<int>();                                                            // CSVへの出力対象
            int diskNo = 1;                                                                         // DiskNo
            long diskSize = 0;                                                                      // Diskの中身
            string outDir = "";                                                                     // 画像の出力先  rootDir \ Disk● \ グループ名
            string query = "SELECT fname, notimgout FROM t_data WHERE gid = :gid ORDER BY fname";   // 画像一覧の取込のクエリー
            foreach (var g in gList)
            {
                using (var r = SQLClass.ExecuteReader(query, "gid", typeof(int), g))
                {
                    while (r.Read())
                    {
                        // CSVへの出力対象を保存する。
                        int fname = Convert.ToInt32(r["fname"]);
                        fList.Add(fname);

                        // 再出力モード以外は、未出力に限定する。   (再出力モード以外・未出力=0の時は画像を出力しない。)
                        if (checkBoxImageReoutput.Checked == false && Convert.ToInt32(r["notimgout"]) == 0) continue;

                        try
                        {
                            // コピー元
                            string from = C_GlobalData.GetFilePath(fname);

                            if (diskSize > 4000000000)
                            {
                                // diskSizeが４ギガ以上 → ディスク交換
                                diskNo++;
                                diskSize = new FileInfo(from).Length;
                            }

                            // 保存先  ●●\img → ROOT\DISK1\img に置換する
                            string dest = from.Replace(C_GlobalData.GetPictureDir(), rootDir + @"\DISK" + diskNo);
                            if (outDir != Path.GetDirectoryName(dest))
                            {
                                outDir = Path.GetDirectoryName(dest);
                                Directory.CreateDirectory(outDir);
                            }

                            File.Copy(from, dest);
                            diskSize += new FileInfo(from).Length;
                        }
                        catch (Exception ex)
                        {
                            File.AppendAllText(C_GlobalData.getAppDir() + @"\err.txt", ex.Message + "\r\n");
                        }
                    }
                }
            }

            
            // CSVデータの作成
		    var lines = new List<string>();
            string[] cells = new string[9];
            int now = DateTime.Now.Year * 10000 + DateTime.Now.Month * 100 + DateTime.Now.Day;
            cells[0] = now.ToString();                                      // 0：データ出力日
            using(var tran = SQLClass.BeginTran())
	        {
                foreach (var f in fList)
                {
                    using (var r = SQLClass.ExecuteReader("SELECT * FROM t_data WHERE fname = :fname", "fname", typeof(string), f))
                    {
                        if (r.Read())
                        {
                            cells[1] = r["fname"].ToString();               // 1：画像ファイル名
                            cells[5] = r["medimonth"].ToString();           // 6：診療年月

                            if (cells[5] != "" && cells[6] != "0" && cells[6] != "9999")
                            {
                                // 診療年月が、０(エラー)、９９９９（続紙）以外
                                cells[2] = r["billmonth"].ToString();       // 2：請求年月
                                cells[3] = r["billname"].ToString();        // 3：支払先
                                cells[4] = r["hosname"].ToString();         // 4：施術院名
                                cells[6] = r["insuredno"].ToString();       // 6：被保険者番号
                                cells[7] = r["insuredname"].ToString();     // 7：患者名
                                cells[8] = r["typecode"].ToString();        // 8：typecode
                            }
                            else
                            {
                                // 診療年月が、０(エラー) または ９９９９（続紙）
                                cells[2] = "";       // 2：請求年月
                                cells[3] = "";       // 3：支払先
                                cells[4] = "";       // 4：施術院名
                                cells[6] = "";       // 6：被保険者番号
                                cells[7] = "";       // 7：患者名
                                cells[8] = "";       // 8：typecode
                            }
                            lines.Add(string.Join(",", cells));

                            SQLClass.ExecuteNonQuery("UPDATE t_data SET notimgout = 0, notoutput = 0 WHERE fname = :fname",
                                                     "fname",  typeof(string), f);
                        }
                    }
                }
                Directory.CreateDirectory(rootDir + @"\disk1\");
                File.WriteAllLines(rootDir + @"\disk1\receiptdata.csv", lines.ToArray(), Encoding.UTF8);
                tran.Commit();
	        }


            // 出力先の表示
            UpdateList();


            System.Diagnostics.Process.Start(rootDir);
            MessageBox.Show("出力完了");
            checkBoxSelectAll.Checked = false;
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count <= 0)
            {
                // データ選択なし
                buttonInout.Enabled = false;
                buttonCheck.Enabled = false;
                lblSelectedGroup.Text = "グループ未選択";
                return;
            }
            else if(Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["notinput"].Value) > 0)
            {
                // 未入力が１件以上のデータを選択した時
                buttonInout.Enabled = true;         // 入力     → 可
                buttonCheck.Enabled = false;        // チェック → 不可（入力完了まで待つ）
            }
            else if (Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["notverify"].Value) >= Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["cnt"].Value))
            {
                // 全て未チェックのデータを選択した時
                buttonInout.Enabled = true;         // 入力     → 可
                buttonCheck.Enabled = true;         // チェック → 可
            }
            else
            {
                // チェック済が１件以上のデータを選択した時
                buttonInout.Enabled = false;        // 入力     → 不可 （既に入力済み・チェックも開始している）
                buttonCheck.Enabled = true;         // チェック → 可
            }


            lblSelectedGroup.Text = "グループ : " + dataGridView1.SelectedRows[0].Cells["gid"].Value + " (" + dataGridView1.SelectedRows[0].Cells["billmonth"].FormattedValue + "請求)" +
                      "\r\n未入力   : " + dataGridView1.SelectedRows[0].Cells["notinput"].Value + " / " + dataGridView1.SelectedRows[0].Cells["cnt"].Value +
                      "\r\n未ﾁｪｯｸ   : " + dataGridView1.SelectedRows[0].Cells["notverify"].Value + " / " + dataGridView1.SelectedRows[0].Cells["cnt"].Value;
        }

        private void checkBoxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            using (var dt = dataGridView1.DataSource as DataTable)
            {
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        row["chk"] = checkBoxSelectAll.Checked && Convert.ToInt32(row["notinput"]) == 0 && Convert.ToInt32(row["notverify"]) == 0;
                    }
                }
            }


        }





    }
}
