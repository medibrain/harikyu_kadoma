﻿namespace WindowsFormsApplication1
{
    partial class FormInput
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormInput));
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelSelection = new System.Windows.Forms.Label();
            this.textBoxInsuredName2 = new System.Windows.Forms.TextBox();
            this.labelGID = new System.Windows.Forms.Label();
            this.textBoxHosname2 = new System.Windows.Forms.TextBox();
            this.textBoxBillname = new System.Windows.Forms.TextBox();
            this.textBoxInsuredName1 = new System.Windows.Forms.TextBox();
            this.buttonCW = new System.Windows.Forms.Button();
            this.textBoxHosname1 = new System.Windows.Forms.TextBox();
            this.buttonCCW = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.Hiho1 = new System.Windows.Forms.TextBox();
            this.buttonNext = new System.Windows.Forms.Button();
            this.Hiho2 = new System.Windows.Forms.TextBox();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonChangeBillmonth = new System.Windows.Forms.Button();
            this.buttonChangeBillname = new System.Windows.Forms.Button();
            this.textbox_MediMonth2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textbox_BillMonth = new System.Windows.Forms.TextBox();
            this.textBoxTypecode2 = new System.Windows.Forms.TextBox();
            this.textBoxTypecode1 = new System.Windows.Forms.TextBox();
            this.textbox_MediMonth1 = new System.Windows.Forms.TextBox();
            this.labelstatus = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox = new WindowsFormsApplication1.UserControlImage();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelSelection);
            this.panel1.Controls.Add(this.textBoxInsuredName2);
            this.panel1.Controls.Add(this.labelGID);
            this.panel1.Controls.Add(this.textBoxHosname2);
            this.panel1.Controls.Add(this.textBoxBillname);
            this.panel1.Controls.Add(this.textBoxInsuredName1);
            this.panel1.Controls.Add(this.buttonCW);
            this.panel1.Controls.Add(this.textBoxHosname1);
            this.panel1.Controls.Add(this.buttonCCW);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.Hiho1);
            this.panel1.Controls.Add(this.buttonNext);
            this.panel1.Controls.Add(this.Hiho2);
            this.panel1.Controls.Add(this.buttonPrev);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.buttonChangeBillmonth);
            this.panel1.Controls.Add(this.buttonChangeBillname);
            this.panel1.Controls.Add(this.textbox_MediMonth2);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textbox_BillMonth);
            this.panel1.Controls.Add(this.textBoxTypecode2);
            this.panel1.Controls.Add(this.textBoxTypecode1);
            this.panel1.Controls.Add(this.textbox_MediMonth1);
            this.panel1.Controls.Add(this.labelstatus);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(766, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(295, 750);
            this.panel1.TabIndex = 0;
            // 
            // labelSelection
            // 
            this.labelSelection.AutoSize = true;
            this.labelSelection.BackColor = System.Drawing.Color.Navy;
            this.labelSelection.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelSelection.ForeColor = System.Drawing.Color.White;
            this.labelSelection.Location = new System.Drawing.Point(95, 423);
            this.labelSelection.Name = "labelSelection";
            this.labelSelection.Size = new System.Drawing.Size(143, 300);
            this.labelSelection.TabIndex = 22;
            this.labelSelection.Text = resources.GetString("labelSelection.Text");
            // 
            // textBoxInsuredName2
            // 
            this.textBoxInsuredName2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxInsuredName2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxInsuredName2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBoxInsuredName2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.textBoxInsuredName2.Location = new System.Drawing.Point(8, 293);
            this.textBoxInsuredName2.Name = "textBoxInsuredName2";
            this.textBoxInsuredName2.Size = new System.Drawing.Size(283, 19);
            this.textBoxInsuredName2.TabIndex = 16;
            this.textBoxInsuredName2.TextChanged += new System.EventHandler(this.SelectionTextbox_TextChanged);
            this.textBoxInsuredName2.Enter += new System.EventHandler(this.textBoxInsuredName_Enter);
            this.textBoxInsuredName2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textbox_KeyDown);
            this.textBoxInsuredName2.Leave += new System.EventHandler(this.SelectionTextbox_Leave);
            // 
            // labelGID
            // 
            this.labelGID.AutoSize = true;
            this.labelGID.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelGID.ForeColor = System.Drawing.Color.Red;
            this.labelGID.Location = new System.Drawing.Point(8, 41);
            this.labelGID.Name = "labelGID";
            this.labelGID.Size = new System.Drawing.Size(53, 60);
            this.labelGID.TabIndex = 1;
            this.labelGID.Text = "labelGID\r\nlabelGID\r\nlabelGID\r\nlabelGID\r\nlabelGID\r\n";
            // 
            // textBoxHosname2
            // 
            this.textBoxHosname2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxHosname2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxHosname2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBoxHosname2.Location = new System.Drawing.Point(6, 398);
            this.textBoxHosname2.Name = "textBoxHosname2";
            this.textBoxHosname2.Size = new System.Drawing.Size(283, 19);
            this.textBoxHosname2.TabIndex = 21;
            this.textBoxHosname2.TextChanged += new System.EventHandler(this.SelectionTextbox_TextChanged);
            this.textBoxHosname2.Enter += new System.EventHandler(this.textBoxHosname_Enter);
            this.textBoxHosname2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textbox_KeyDown);
            this.textBoxHosname2.Leave += new System.EventHandler(this.SelectionTextbox_Leave);
            // 
            // textBoxBillname
            // 
            this.textBoxBillname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxBillname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxBillname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBoxBillname.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.textBoxBillname.Location = new System.Drawing.Point(6, 340);
            this.textBoxBillname.Name = "textBoxBillname";
            this.textBoxBillname.Size = new System.Drawing.Size(283, 19);
            this.textBoxBillname.TabIndex = 18;
            this.textBoxBillname.TextChanged += new System.EventHandler(this.SelectionTextbox_TextChanged);
            this.textBoxBillname.Enter += new System.EventHandler(this.textBoxBillname_Enter);
            this.textBoxBillname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textbox_KeyDown);
            this.textBoxBillname.Leave += new System.EventHandler(this.SelectionTextbox_Leave);
            // 
            // textBoxInsuredName1
            // 
            this.textBoxInsuredName1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxInsuredName1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxInsuredName1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBoxInsuredName1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.textBoxInsuredName1.Location = new System.Drawing.Point(8, 274);
            this.textBoxInsuredName1.Name = "textBoxInsuredName1";
            this.textBoxInsuredName1.Size = new System.Drawing.Size(283, 19);
            this.textBoxInsuredName1.TabIndex = 15;
            this.textBoxInsuredName1.TextChanged += new System.EventHandler(this.SelectionTextbox_TextChanged);
            this.textBoxInsuredName1.Enter += new System.EventHandler(this.textBoxInsuredName_Enter);
            this.textBoxInsuredName1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textbox_KeyDown);
            this.textBoxInsuredName1.Leave += new System.EventHandler(this.SelectionTextbox_Leave);
            // 
            // buttonCW
            // 
            this.buttonCW.Image = global::WindowsFormsApplication1.Properties.Resources.ImageClockwise;
            this.buttonCW.Location = new System.Drawing.Point(49, 512);
            this.buttonCW.Name = "buttonCW";
            this.buttonCW.Size = new System.Drawing.Size(46, 46);
            this.buttonCW.TabIndex = 26;
            this.buttonCW.TabStop = false;
            this.buttonCW.UseVisualStyleBackColor = true;
            this.buttonCW.Click += new System.EventHandler(this.buttonCW_Click);
            // 
            // textBoxHosname1
            // 
            this.textBoxHosname1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxHosname1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxHosname1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBoxHosname1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.textBoxHosname1.Location = new System.Drawing.Point(6, 379);
            this.textBoxHosname1.Name = "textBoxHosname1";
            this.textBoxHosname1.Size = new System.Drawing.Size(283, 19);
            this.textBoxHosname1.TabIndex = 20;
            this.textBoxHosname1.TextChanged += new System.EventHandler(this.SelectionTextbox_TextChanged);
            this.textBoxHosname1.Enter += new System.EventHandler(this.textBoxHosname_Enter);
            this.textBoxHosname1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textbox_KeyDown);
            this.textBoxHosname1.Leave += new System.EventHandler(this.SelectionTextbox_Leave);
            // 
            // buttonCCW
            // 
            this.buttonCCW.Image = global::WindowsFormsApplication1.Properties.Resources.ImageCounterclockwise;
            this.buttonCCW.Location = new System.Drawing.Point(3, 512);
            this.buttonCCW.Name = "buttonCCW";
            this.buttonCCW.Size = new System.Drawing.Size(46, 46);
            this.buttonCCW.TabIndex = 25;
            this.buttonCCW.TabStop = false;
            this.buttonCCW.UseVisualStyleBackColor = true;
            this.buttonCCW.Click += new System.EventHandler(this.buttonCCW_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 228);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "被保険者番号";
            // 
            // Hiho1
            // 
            this.Hiho1.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Hiho1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Hiho1.Location = new System.Drawing.Point(85, 223);
            this.Hiho1.MaxLength = 6;
            this.Hiho1.Name = "Hiho1";
            this.Hiho1.Size = new System.Drawing.Size(75, 22);
            this.Hiho1.TabIndex = 12;
            this.Hiho1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textbox_KeyDown);
            // 
            // buttonNext
            // 
            this.buttonNext.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.buttonNext.Location = new System.Drawing.Point(6, 465);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(77, 36);
            this.buttonNext.TabIndex = 24;
            this.buttonNext.Text = "次へ";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // Hiho2
            // 
            this.Hiho2.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Hiho2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Hiho2.Location = new System.Drawing.Point(160, 223);
            this.Hiho2.MaxLength = 6;
            this.Hiho2.Name = "Hiho2";
            this.Hiho2.Size = new System.Drawing.Size(75, 22);
            this.Hiho2.TabIndex = 13;
            this.Hiho2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textbox_KeyDown);
            // 
            // buttonPrev
            // 
            this.buttonPrev.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.buttonPrev.Location = new System.Drawing.Point(6, 423);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(77, 36);
            this.buttonPrev.TabIndex = 23;
            this.buttonPrev.TabStop = false;
            this.buttonPrev.Text = "戻る";
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 259);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 14;
            this.label9.Text = "患者氏名";
            // 
            // buttonChangeBillmonth
            // 
            this.buttonChangeBillmonth.Location = new System.Drawing.Point(6, 104);
            this.buttonChangeBillmonth.Name = "buttonChangeBillmonth";
            this.buttonChangeBillmonth.Size = new System.Drawing.Size(75, 23);
            this.buttonChangeBillmonth.TabIndex = 2;
            this.buttonChangeBillmonth.TabStop = false;
            this.buttonChangeBillmonth.Text = "請求月変更";
            this.buttonChangeBillmonth.UseVisualStyleBackColor = true;
            this.buttonChangeBillmonth.Click += new System.EventHandler(this.buttonChangeBillmonth_Click);
            // 
            // buttonChangeBillname
            // 
            this.buttonChangeBillname.Location = new System.Drawing.Point(6, 317);
            this.buttonChangeBillname.Name = "buttonChangeBillname";
            this.buttonChangeBillname.Size = new System.Drawing.Size(75, 23);
            this.buttonChangeBillname.TabIndex = 17;
            this.buttonChangeBillname.TabStop = false;
            this.buttonChangeBillname.Text = "支払先変更";
            this.buttonChangeBillname.UseVisualStyleBackColor = true;
            this.buttonChangeBillname.Click += new System.EventHandler(this.buttonChangeBillname_Click);
            // 
            // textbox_MediMonth2
            // 
            this.textbox_MediMonth2.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textbox_MediMonth2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textbox_MediMonth2.Location = new System.Drawing.Point(158, 132);
            this.textbox_MediMonth2.MaxLength = 4;
            this.textbox_MediMonth2.Name = "textbox_MediMonth2";
            this.textbox_MediMonth2.Size = new System.Drawing.Size(75, 22);
            this.textbox_MediMonth2.TabIndex = 6;
            this.textbox_MediMonth2.TextChanged += new System.EventHandler(this.textbox_MediMonth2_TextChanged);
            this.textbox_MediMonth2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textbox_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 367);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 19;
            this.label2.Text = "施術院名";
            // 
            // textbox_BillMonth
            // 
            this.textbox_BillMonth.Enabled = false;
            this.textbox_BillMonth.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textbox_BillMonth.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textbox_BillMonth.Location = new System.Drawing.Point(83, 104);
            this.textbox_BillMonth.MaxLength = 4;
            this.textbox_BillMonth.Name = "textbox_BillMonth";
            this.textbox_BillMonth.Size = new System.Drawing.Size(75, 22);
            this.textbox_BillMonth.TabIndex = 3;
            this.textbox_BillMonth.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textbox_KeyDown);
            // 
            // textBoxTypecode2
            // 
            this.textBoxTypecode2.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxTypecode2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxTypecode2.Location = new System.Drawing.Point(158, 179);
            this.textBoxTypecode2.MaxLength = 1;
            this.textBoxTypecode2.Name = "textBoxTypecode2";
            this.textBoxTypecode2.Size = new System.Drawing.Size(38, 22);
            this.textBoxTypecode2.TabIndex = 10;
            this.textBoxTypecode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textbox_KeyDown);
            // 
            // textBoxTypecode1
            // 
            this.textBoxTypecode1.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxTypecode1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxTypecode1.Location = new System.Drawing.Point(120, 179);
            this.textBoxTypecode1.MaxLength = 1;
            this.textBoxTypecode1.Name = "textBoxTypecode1";
            this.textBoxTypecode1.Size = new System.Drawing.Size(38, 22);
            this.textBoxTypecode1.TabIndex = 9;
            this.textBoxTypecode1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textbox_KeyDown);
            // 
            // textbox_MediMonth1
            // 
            this.textbox_MediMonth1.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textbox_MediMonth1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textbox_MediMonth1.Location = new System.Drawing.Point(83, 132);
            this.textbox_MediMonth1.MaxLength = 4;
            this.textbox_MediMonth1.Name = "textbox_MediMonth1";
            this.textbox_MediMonth1.Size = new System.Drawing.Size(75, 22);
            this.textbox_MediMonth1.TabIndex = 5;
            this.textbox_MediMonth1.TextChanged += new System.EventHandler(this.textbox_MediMonth1_TextChanged);
            this.textbox_MediMonth1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textbox_KeyDown);
            // 
            // labelstatus
            // 
            this.labelstatus.AutoSize = true;
            this.labelstatus.BackColor = System.Drawing.Color.Black;
            this.labelstatus.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelstatus.ForeColor = System.Drawing.Color.White;
            this.labelstatus.Location = new System.Drawing.Point(8, 9);
            this.labelstatus.Name = "labelstatus";
            this.labelstatus.Size = new System.Drawing.Size(116, 24);
            this.labelstatus.TabIndex = 0;
            this.labelstatus.Text = "labelstatus";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(83, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "続紙:[----]  エラー:[****]";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "区分（１鍼 ２あんま）";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 137);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "診療年月";
            // 
            // pictureBox
            // 
            this.pictureBox.AutoScroll = true;
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Image = null;
            this.pictureBox.ImageLocation = null;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(766, 750);
            this.pictureBox.TabIndex = 1;
            // 
            // FormInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1061, 750);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1000, 600);
            this.Name = "FormInput";
            this.Text = "鍼灸・柔整レセプト入力システム";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormInput_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textbox_MediMonth2;
        private System.Windows.Forms.TextBox textbox_MediMonth1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Hiho2;
        private System.Windows.Forms.TextBox Hiho1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonChangeBillname;
        private System.Windows.Forms.Label labelstatus;
        private UserControlImage pictureBox;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrev;
        private System.Windows.Forms.Button buttonCCW;
        private System.Windows.Forms.Button buttonCW;
        private System.Windows.Forms.TextBox textbox_BillMonth;
        private System.Windows.Forms.Button buttonChangeBillmonth;
        private System.Windows.Forms.Label labelGID;
        private System.Windows.Forms.TextBox textBoxHosname2;
        private System.Windows.Forms.TextBox textBoxHosname1;
        private System.Windows.Forms.TextBox textBoxBillname;
        private System.Windows.Forms.TextBox textBoxInsuredName2;
        private System.Windows.Forms.TextBox textBoxInsuredName1;
        private System.Windows.Forms.Label labelSelection;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxTypecode2;
        private System.Windows.Forms.TextBox textBoxTypecode1;
    }
}

