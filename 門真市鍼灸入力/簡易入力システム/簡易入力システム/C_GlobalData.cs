﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using System.Data.Common;
#if POSTGRESQL
using Npgsql;
#elif SQLIGHT
using System.Data.SQLite;
#endif
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace WindowsFormsApplication1
{
    /// <summary>
    /// グローバルなデータを格納するクラス
    /// </summary>
    class C_GlobalData
    {

        [DllImport("Imm32.dll", EntryPoint = "ImmGetContext")]
        private extern static IntPtr APIImmGetContext(IntPtr hWnd);

        [DllImport("Imm32.dll", EntryPoint = "ImmNotifyIME")]
        private extern static bool APIImmNotifyIME(IntPtr hIMC, NI dwAction, CPS dwIndex, uint dwValue);

        [DllImport("Imm32.dll", EntryPoint = "ImmReleaseContext")]
        private extern static bool APIImmReleaseContext(IntPtr hWnd, IntPtr hIMC);

        private enum NI : uint
        {
            OPENCANDIDATE = 0x0010,
            CLOSECANDIDATE = 0x0011,
            SELECTCANDIDATESTR = 0x0012,
            CHANGECANDIDATELIST = 0x0013,
            FINALIZECONVERSIONRESULT = 0x0014,
            COMPOSITIONSTR = 0x0015,
            SETCANDIDATE_PAGESTART = 0x0016,
            SETCANDIDATE_PAGESIZE = 0x0017,
            IMEMENUSELECTED = 0x0018,
        }

        private enum CPS : uint
        {
            COMPLETE = 0x0001,
            CONVERT = 0x0002,
            REVERT = 0x0003,
            CANCEL = 0x0004,
        }




        /// <summary>
        /// 自分自身のアプリのパスを取得する
        /// </summary>
        static public string getAppDir()
        {
            string path = System.Reflection.Assembly.GetEntryAssembly().Location;
            return Path.GetDirectoryName(path);
        }

        /// <summary> INIファイル情報保存用 </summary>
        static string[] iniData = null;
 
        /// <summary> INIファイル情報を読み込む </summary>
        /// <returns></returns>
        public static string[] GetIniData()
        {
            if(iniData==null) iniData = File.ReadAllLines(getAppDir() + @"\SQL.ini");
            return iniData;
        }

        /// <summary> 画像ファイル、SQLight保存用フォルダ の取得</summary>
        /// <returns></returns>
        public static string GetPictureDir()
        {
            // INIファイルの1行目 但し * → EXEと同じ
            return GetIniData()[0].Replace("*", getAppDir()).Trim();
        }

#if POSTGRESQL
        /// <summary> PostgreSql用ConnectTextの取得</summary>
        /// <returns></returns>
        public static string ConnectText()
        {
            string[] l = GetIniData();
            string s = "";
            for (int i = 1; i < l.Length; i++) s += l[i] + "\r\n";

            return s;
        }
#endif
        /// <summary> DBに登録されたファイル名を取得 </summary>
        /// <param name="filenum">ファイル番号</param>
        /// <returns>フルパス名</returns>
        public static string GetFilePath(int filenum)
        {
            string name = filenum.ToString("000000000") + ".gif";
            return GetPictureDir() + @"\img\" + name.Substring(0,3) + @"\"  + name.Substring(0,6) + @"\" +name;
        }



        /// <summary> textboxの入力補完のプレビュー・フォーカス移動時に入力補完を更新する。 </summary>
        /// <param name="textbox">textbox</param>
        /// <param name="labelSelection">表示用Label</param>
        /// <param name="sqlCommand">sqlCommand</param>
        /// <param name="sqlParams">sqlParams</param>
        public static void SelectionTextboxPreviuew(TextBox textbox, Label labelSelection, string sqlCommand, params object[] sqlParams)
        {
            // labelの表示
            using(var r = SQLClass.ExecuteReader(sqlCommand, sqlParams))
            {
                while(r.Read())
                {
                    if(r[0].ToString().Trim().Length>0) textbox.AutoCompleteCustomSource.Add(r[0].ToString().Trim());
                }
            }
                        
            string labelText = "";
            for (int i = 0; i < Math.Min(textbox.AutoCompleteCustomSource.Count, 20); i++) labelText += (i+1).ToString() + "." + textbox.AutoCompleteCustomSource[i] + "\r\n";
            labelSelection.Text = labelText;
            if (textbox.AutoCompleteCustomSource.Count > 0)
            {
                //labelSelection.Location = new System.Drawing.Point(textbox.Location.X, textbox.Location.Y + textbox.Height);
                labelSelection.Visible = true;
            }
            else
            {
                labelSelection.Visible = false;
            }
        }

        public static void SelectionTextboxChanged(TextBox textbox)
        {
            // IME制御 （強制的に確定させる）
            IntPtr hIMC = APIImmGetContext(textbox.Handle);
            bool INI = APIImmNotifyIME(hIMC, NI.COMPOSITIONSTR, CPS.COMPLETE, 0);
            bool IRC = APIImmReleaseContext(textbox.Handle, hIMC);
            Application.DoEvents();

            string txt = Microsoft.VisualBasic.Strings.StrConv(textbox.Text.Trim(), Microsoft.VisualBasic.VbStrConv.Narrow);
            //if (txt.Length <= 0) return;


            int n = 0;
            if (txt.Length >= 2 && n<=0)  
            {
                if(int.TryParse( txt.Substring(0,2) , out n) == false) n=0;
            }            
            if (txt.Length >= 1 && n<=0)  
            {
                if(int.TryParse( txt.Substring(0,1) , out n) == false) n=0;
            }
                        
                
            if (n >= 1 && n <= textbox.AutoCompleteCustomSource.Count)
            {
                textbox.Text = textbox.AutoCompleteCustomSource[n - 1];
            }

            //char c = txt[0];

            //int n;
            //if (c >= '1' && c <= '9') n = c - '1';
            //else if (c >= '１' && c <= '９') n = c - '１';
            //else n = -1;

            ////System.Diagnostics.Debug.WriteLine(txt);
            ////System.Diagnostics.Debug.WriteLine(n);


            //if (n >= 0 && n < textbox.AutoCompleteCustomSource.Count)
            //{
            //}
            //else
            //{
            //    txt = txt.TrimStart("0123456789０１２３４５６７８９".ToCharArray());
            //    if (textbox.Text != txt) textbox.Text = txt;
            //}
        }
    }

}
