﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class FormScan : Form
    {
        public FormScan()
        {
            InitializeComponent();
        }

        private void FormScan_Load(object sender, EventArgs e)
        {
            labelSelection.Text = "";
            buttonCCW.Image = Properties.Resources.ImageCounterclockwise;
            buttonCW.Image = Properties.Resources.ImageClockwise;

            labelCount.Text = "";
            labelProg.Text = "";
        }
        
        /// <summary> 対象フォルダ選択 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSelectDir_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog f = new FolderBrowserDialog())
            {
                f.ShowNewFolderButton = false;
                var res = f.ShowDialog();
                if (res != System.Windows.Forms.DialogResult.OK) return;
                if (Directory.Exists(f.SelectedPath) == false) return;

                textBoxDir.Text = f.SelectedPath;
            }


            // 画像一覧の更新
            m_PictureList.Clear();
            foreach (string p in "*.png,*gif,*.bmp,*tiff,*.tif,*.jpg,*.jpeg".Split(','))
            {
                foreach (var f in Directory.GetFiles(textBoxDir.Text, p))
                {
                    if (m_PictureList.Contains(f) == false) m_PictureList.Add(f);
                }
            }

            // プレビュー
            m_PreviewIndex = 0;
            ShowImagePreview();

            labelCount.Text = m_PictureList.Count.ToString() + "枚";
            m_Angle = 0;

            panel1.Enabled = true;
            pictureBox1.Enabled = true;

            textbox_billdate.Select();
        }


        /// <summary> 表示角度 </summary>
        int m_Angle = 0;

        /// <summary> 表示画像 </summary>
        int m_PreviewIndex = 0;

        /// <summary> 画像一覧(フルパス) </summary>
        List<string> m_PictureList = new List<string>();
        
        /// <summary> 請求年月 </summary>
        int m_BillMonth;

        /// <summary> 支払先名 </summary>
        string m_BillName = "";

        /// <summary> 画像プレビューの表示 </summary>
        void ShowImagePreview()
        {
            Image img;
            
            if (pictureBox1.Image != null)
            {
                // 既に表示されている画像を消去
                img = pictureBox1.Image;
                pictureBox1.Image = null;
                img.Dispose();
            }

            // 画像番号のチェック
            if (m_PictureList.Count <= 0) return;
            if(m_PreviewIndex < 0) m_PreviewIndex = 0;
            else if(m_PreviewIndex >= m_PictureList.Count) m_PreviewIndex = m_PictureList.Count-1;

            // READ
            using (var fs = File.OpenRead(m_PictureList[m_PreviewIndex]))
            {
                img = Image.FromStream(fs);
                fs.Close();
                
            }


            // 回転
            Rotate(img, m_Angle);

            // 表示
            pictureBox1.Image = img;
        }

        /// <summary> 画像の回転 </summary>
        /// <param name="img">画像</param>
        /// <param name="angle">角度（0, 90, 180, 270 時計回りに回転させる）</param>
        void Rotate(Image img, int angle)
        {
            switch (angle)
            {
                case 90:
                    img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    break;
                case 180:
                    img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    break;
                case 270:
                    img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    break;
                default:
                    break;
            }
        }

        /// <summary> 反時計回りにプレビューを90度回転 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCCW_Click(object sender, EventArgs e)
        {
            m_Angle = (m_Angle + 270) % 360;
            ShowImagePreview();
        }


        /// <summary> 時計回りにプレビューを90度回転 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCW_Click(object sender, EventArgs e)
        {
            m_Angle = (m_Angle + 90) % 360;
            ShowImagePreview();
        }
        
        private void buttonPrev_Click(object sender, EventArgs e)
        {
            m_PreviewIndex--;
            ShowImagePreview();
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            m_PreviewIndex++;
            ShowImagePreview();
        }


        /// <summary> スキャンの実行 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonScan_Click(object sender, EventArgs e)
        {
            if (m_PictureList.Count <= 0)
            {
                return;
            }


            // 年度チェック
            //20190613144751 furukawa st ////////////////////////
            //年度チェック令和対応
            
            if (int.TryParse(textbox_billdate.Text, out m_BillMonth) == false ||
                m_BillMonth < 105 || m_BillMonth % 100 < 1 || m_BillMonth % 100 > 12)

                        //if (int.TryParse(textbox_billdate.Text, out m_BillMonth) == false ||
                        //m_BillMonth < 2000 || m_BillMonth % 100 < 1 || m_BillMonth % 100 > 12)
            //20190613144751 furukawa ed ////////////////////////

            {
                textbox_billdate.Select();
                textbox_billdate.SelectAll();

                //20190613144828 furukawa st ////////////////////////
                //年度チェックメッセージ令和対応                
                MessageBox.Show("YYMM形式（和暦年）で、請求年月を入力してください。");
                    //MessageBox.Show("YYMM形式（平成年）で、請求年月を入力してください。");
                //20190613144828 furukawa ed ////////////////////////


                return;
            }

            // 支払先漢字チェック
            string billname = textBox_billname.Text.Trim();

            if (billname.Length <= 0)
            {
                textBox_billname.Select();
                textBox_billname.SelectAll();
                MessageBox.Show("支払先名を入力してください");
                return;
            }
            m_BillName = billname;


            panel0.Enabled = false;
            panel1.Enabled = false;
            pictureBox1.Enabled = false;

            backgroundWorker1.RunWorkerAsync();

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                //  gidを取得 （最新＋１）
                int gid ;
                using (var r = SQLClass.ExecuteReader("SELECT MAX(gid) FROM t_data"))
                {
                    if (r.Read() == false) gid = 1;
                    else if (r[0].GetType() == typeof(DBNull)) gid = 1;
                    else gid = Convert.ToInt32(r[0]) + 1;
                }


                string dir = "";        // 取り込み先ディレクトリ

                // INSERTコマンドの発行 (ファイルの数だけ)
                string query = "INSERT INTO t_data (gid, sdate, billmonth, billname) VALUES (:gid, :sdate, :billmonth, :billname)";
#if POSTGRESQL
                query += "RETURNING fname";
#elif SQLIGHT
                query += ";SELECT MAX(fname) FROM t_data";
#endif
                var cmd = SQLClass.GetDbCommand(query,
                                                "gid", typeof(int), gid,
                                                "sdate", typeof(string), DateTime.Now.Year * 10000 + DateTime.Now.Month * 100 + DateTime.Now.Day, 
                                                "billmonth", typeof(int), m_BillMonth,
                                                "billname", typeof(string), m_BillName);

                for (int i = 0; i < m_PictureList.Count; i++)
                {
                    backgroundWorker1.ReportProgress(100 * i / m_PictureList.Count, i.ToString() + " / " + m_PictureList.Count);

                    using (var r = cmd.ExecuteReader())
                    {
                        // 挿入実行・挿入後のファイル番号を保持する。
                        r.Read();


                        string from = m_PictureList[i];
                        string dest = C_GlobalData.GetFilePath(Convert.ToInt32(r[0]));

                        // ファイルなし → 移動不可！
                        if (File.Exists(from) == false) continue;

                        // 上書き前に移動先を削除
                        if (File.Exists(dest)) File.Delete(dest);

                        // ディレクトリ作成
                        if (Path.GetDirectoryName(dest) != dir)
                        {
                            dir = Path.GetDirectoryName(dest);
                            Directory.CreateDirectory(dir);
                        }


                        var filesize = new System.IO.FileInfo(from).Length;
                        if (filesize < 200000 && m_Angle == 0)
                        {
                            // 200KB未満・回転なし → ファイル移動のみを行う。
                            File.Move(from, dest);
                        }
                        else
                        {
                            // 200KB以上 or 回転あり → 画像処理が必要。                    
                            using (Image img = Image.FromFile(from))
                            {
                                Rotate(img, m_Angle);

                                // 出力フォーマット 　200KB (200000B)以上：GIFに圧縮 ／ 未満：そのまま（スキャナが圧縮した画像を使う）
                                img.Save(dest, (filesize >= 200000) ? (ImageFormat.Gif) : img.RawFormat);
                            }
                            File.Delete(from);
                        }


                    }
                }
                e.Result = "取込完了";

            }
            catch (Exception ex)
            {
                e.Result = ex.Message;
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                progressBar1.Value = e.ProgressPercentage;
                labelProg.Text = e.UserState.ToString();
            }
            catch (Exception)
            {
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show(e.Result.ToString());
            Close();
        }

        private void FormScan_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = backgroundWorker1.IsBusy;
        }

        /// <summary> 請求月変更時</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textbox_billdate_TextChanged(object sender, EventArgs e)
        {
            var textbox = sender as TextBox;
            int billmomth;
            if (int.TryParse(textbox.Text, out billmomth) == false) { }
            
            //20190613144951 furukawa st ////////////////////////
            //令和対応            
            else if (billmomth > 104 && billmomth % 100 >= 1 && billmomth % 100 <= 12) { this.SelectNextControl(textbox, true, true, true, false); }
                //else if (billmomth > 2000 && billmomth % 100 >= 1 && billmomth % 100 <= 12) { this.SelectNextControl(textbox, true, true, true, false); }
            //20190613144951 furukawa ed ////////////////////////
        }

        private void textBox_billname_TextChanged(object sender, EventArgs e)
        {
            C_GlobalData.SelectionTextboxChanged(sender as TextBox);
        }

        private void textBoxDate_Enter(object sender, EventArgs e)
        {

        }

        bool textBox_EnterEnable = true;
        private void textBox_Enter(object sender, EventArgs e)
        {
            if (textBox_EnterEnable == false) return;
            textBox_EnterEnable = false;
            

            var t = sender as TextBox;
            t.AutoCompleteCustomSource.Clear();
            C_GlobalData.SelectionTextboxPreviuew(t, labelSelection, "SELECT billname FROM t_data GROUP BY billname ORDER BY COUNT(*) DESC");

            if (t == null) return;
            else if (t.ReadOnly == true || t.Enabled == false){}
            else
            {
                t.BackColor = Color.Yellow;
                t.ForeColor = Color.Black;
                t.SelectAll();
            }


            textBox_EnterEnable = true;
        }

        private void textBox_Leave(object sender, EventArgs e)
        {
            labelSelection.Text = "";
            var t = sender as TextBox;
            if (t == null) return;
            else if (t.ReadOnly == true || t.Enabled == false) return;
            else
            {
                t.BackColor = Color.White;
                t.ForeColor = Color.Black;
            }
        }




    }
}
