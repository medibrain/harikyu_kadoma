﻿//#define BETA    // βバージョン （所要時間調査）
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Drawing.Printing;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Runtime.InteropServices;

namespace WindowsFormsApplication1
{   
    /// <summary>
    /// 柔整レセプト検索システムのメイン画面のクラス
    /// </summary>
    public partial class FormInput : Form
    {
        /// <summary> IMEが開いて居るか何うかを調べる。</summary>
        /// <param name="hIMC">入力コンテキストのハンドルを指定</param>
        /// <returns>IMEが開いて居れば0以外の値が返り、其れ以外の場合は0が返る。</returns>
        [DllImport("Imm32.dll")]
        private static extern int ImmGetOpenStatus(IntPtr hIMC);


        enum eStatus
        {
            エラー,
            入力未,
            入力済,
            チェック未,
            チェック済,
        };
        
        /// <summary> 入力未, 入力済, チェック未, チェック済 </summary>
        eStatus mStatus
        {
            get
            {
                eStatus e;
                if (Enum.TryParse<eStatus>(labelstatus.Text, out e) == true) return e;
                else return eStatus.エラー;
            }
            set
            {
                labelstatus.Text = value.ToString();
                switch (value)
                {
                    case eStatus.入力未:
                        labelstatus.ForeColor = Color.Blue;
                        labelstatus.BackColor = this.BackColor;
                        break;
                    case eStatus.入力済:
                        labelstatus.ForeColor = Color.White;
                        labelstatus.BackColor = Color.Blue;
                        break;
                    case eStatus.チェック未:
                        labelstatus.ForeColor = Color.Red;
                        labelstatus.BackColor = this.BackColor;
                        break;
                    case eStatus.チェック済:
                        labelstatus.ForeColor = Color.White;
                        labelstatus.BackColor = Color.Red;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary> グループID </summary>
        int m_GID;

        /// <summary> false入力モード、trueチェックモード</summary>
        bool m_ISVERIFY;

        /// <summary> 現在選択しているファイル </summary>
        int m_FNAME
        {
            get
            {
                try
                {
                    return int.Parse(Path.GetFileNameWithoutExtension(pictureBox.ImageLocation));
                }
                catch (Exception)
                {
                    return -1;
                }
            }

            set
            {
                pictureBox.ImageLocation = (value >= 0 ? C_GlobalData.GetFilePath(value) : "");
            }
        }

        /// <summary></summary>
        /// <param name="gid"></param>
        public FormInput(int gid, bool isVerify)
        {
            m_GID = gid;
            m_ISVERIFY = isVerify;
            InitializeComponent();
        }            

        private void FormInput_Load(object sender, EventArgs e)
        {  
            m_FNAME = -1;
            mStatus = eStatus.エラー;
            labelSelection.Text = "";

            if (ShowFirstData() == false)
            {
                MessageBox.Show("データなし");
                Close();
            }            
        }

        bool ShowFirstData()
        {
            // 未ベリファイ・未入力から、最初のデータを探す
            string query;
            if (!m_ISVERIFY) query = "SELECT * from t_data" +
                                     " WHERE gid = :gid AND notinput = 1" +                       // 入力未（notinput = 1）から探す
                                     " ORDER BY fname LIMIT 1";                                   // ファイル名順
            else query = "SELECT * from t_data" +
                         " WHERE gid = :gid AND notinput = 0 AND notverify = 1 " +    // 入力済（notinput = 0）・チェック済（notverify = 1）から探す
                         " ORDER BY fname LIMIT 1";                                   // ファイル名順
            using (var r = SQLClass.ExecuteReader(query, "gid", typeof(int), m_GID))
            {
                if (r.Read() == true) return ShowData(r);
            }

            // ベリファイ済・入力済から、次のデータを探す
            if (!m_ISVERIFY) query = "SELECT * from t_data" +
                                     " WHERE gid = :gid AND fname > :fname ORDER BY fname LIMIT 1";
            else query = "SELECT * from t_data" +
                         " WHERE gid = :gid AND fname > :fname AND notinput = 0 ORDER BY fname LIMIT 1";
            using (var r = SQLClass.ExecuteReader(query, "gid", typeof(int), m_GID, "fname", typeof(int), m_FNAME))
            {
                if (r.Read() == true) return ShowData(r);
            }
            return false;
        }


        bool ShowNextData()
        {
            // 次のデータを探す
            string query;
            if (!m_ISVERIFY) query = "SELECT * from t_data WHERE gid = :gid AND fname > :fname ORDER BY fname LIMIT 1";
            else             query = "SELECT * from t_data WHERE gid = :gid AND fname > :fname AND notinput = 0 ORDER BY fname LIMIT 1";
            using (var r = SQLClass.ExecuteReader(query, "gid", typeof(int), m_GID, "fname", typeof(int), m_FNAME))
            {
                if (r.Read() == true) return ShowData(r);
            }
            return false;
        }
        
        bool ShowPrevData()
        {
            // ひとつ前のデータを探す
            string query;
            if (!m_ISVERIFY) query = "SELECT * from t_data WHERE gid = :gid AND fname < :fname ORDER BY fname DESC LIMIT 1";
            else query = "SELECT * from t_data WHERE gid = :gid AND fname < :fname AND notinput = 0 ORDER BY fname DESC LIMIT 1";
            using (var r = SQLClass.ExecuteReader(query, "gid", typeof(int), m_GID, "fname", typeof(int), m_FNAME))
            {
                if (r.Read() == true) return ShowData(r);
            }
            return false;
        }
        
        bool ShowData(DbDataReader r)
        {
            System.Diagnostics.Debug.WriteLine("ShowData 開始");

            m_FNAME = Convert.ToInt32(r["fname"]);

            // 現在のグループ情報と、ファイル番号を表示する。
            using (var r2 = SQLClass.ExecuteReader("SELECT * FROM V_GROUP WHERE gid = :gid", "gid", typeof(int), m_GID))
            {
                if (r2.Read())
                {
                    labelGID.Text = "グループID : " + m_GID.ToString("0000") +
                                "\r\nファイル名 : " + r["fname"].ToString().PadLeft(8, '0') + 
                                "\r\n            (" + r2["minfn"].ToString().PadLeft(8, '0') + "～" + r2["maxfn"].ToString().PadLeft(8, '0') + ")" +
                                "\r\n未入力     : " + r2["notinput"].ToString().PadLeft(5) + " / " + r2["cnt"].ToString().PadLeft(5) +
                                "\r\n未チェック : " + r2["notverify"].ToString().PadLeft(5) + " / " + r2["cnt"].ToString().PadLeft(5);
                }
            }

            // ステータスの更新
            if (m_ISVERIFY == false) mStatus = (Convert.ToInt32(r["notinput"]) > 0 ? eStatus.入力未 : eStatus.入力済);
            else mStatus = (Convert.ToInt32(r["notverify"]) > 0 ? eStatus.チェック未 : eStatus.チェック済);


            // 請求年月・支払先の表示
            textbox_BillMonth.Text = r["billmonth"].ToString();
            textBoxBillname.Text = r["billname"].ToString();

            
            // 支払先→施術院名の入力補完
            InitHosNameList();


            string month = r["medimonth"].ToString();
            if (month == "0") month = "****";             // 0000 → **** （エラー） と表示
            else if (month == "9999") month = "----";     // 9999 → ---- （続紙）   と表示

            textBoxTypecode2.Text = r["typecode"].ToString();           // typecode
            textbox_MediMonth2.Text = month;
            textBoxInsuredName2.Text = r["insuredname"].ToString();     // 患者氏名
            string h = r["insuredno"].ToString();                       // 被保険者番号
            if (h.Length > 0) h = h.PadLeft(Hiho2.MaxLength, '0');
            Hiho2.Text = h;      
            textBoxHosname2.Text = r["hosname"].ToString();             // 施術院名


            if (mStatus != eStatus.チェック未)
            {
                textBoxTypecode1.Text = textBoxTypecode2.Text;
                textbox_MediMonth1.Text = textbox_MediMonth2.Text;                     // 診療年月
                textBoxInsuredName1.Text = textBoxInsuredName2.Text;                   // 患者氏名
                Hiho1.Text = Hiho2.Text;                                               // 被保険者番号
                textBoxHosname1.Text = textBoxHosname2.Text;                           // 施術院名
            }
            else
            {
                textBoxTypecode1.Text = "";
                textbox_MediMonth1.Text = "";                     // 診療年月
                textBoxInsuredName1.Text = "";                    // 患者氏名
                Hiho1.Text = "";                                  // 被保険者番号
                textBoxHosname1.Text = "";                        // 施術院名
            }

                                   
            // ベリファイ用の入力テキストを隠す
            textBoxTypecode2.Visible = false;
            textbox_MediMonth2.Visible = false;
            textBoxInsuredName2.Visible = false;
            Hiho2.Visible = false;
            textBoxHosname2.Visible = false;

            // 審査月・請求先は入力禁止
            textbox_BillMonth.Enabled = false;
            textBoxBillname.Enabled = false;
            
            textbox_MediMonth1.Select();
            textbox_MediMonth1.SelectAll();

            System.Diagnostics.Debug.WriteLine("ShowData TRUE");
            return true;
        }

        /// <summary> 支払先→施術院名の入力補完を更新する </summary>
        void InitHosNameList()
        {
            using (var r2 = SQLClass.ExecuteReader("SELECT hosname FROM t_data WHERE billname = :billname AND NOT (hosname = billname) GROUP BY hosname  ORDER BY COUNT(*) DESC",
                                                   "billname", typeof(string), textBoxBillname.Text.Trim()))
            {
                textBoxHosname1.AutoCompleteCustomSource.Clear();
                textBoxHosname2.AutoCompleteCustomSource.Clear();
                textBoxHosname1.AutoCompleteCustomSource.Add(textBoxBillname.Text.Trim());
                textBoxHosname2.AutoCompleteCustomSource.Add(textBoxBillname.Text.Trim());
                while (r2.Read())
                {
                    textBoxHosname1.AutoCompleteCustomSource.Add(r2[0].ToString());
                    textBoxHosname2.AutoCompleteCustomSource.Add(r2[0].ToString());
                }
            }
        }

        /// <summary> 入力チェック ・ DB更新 （入力エラー時は、例外を発生させる！）</summary>
        void DBWrite()
        {

            System.Diagnostics.Debug.WriteLine("DBWrite");

            // 請求月のチェック
            int billmonth;      
            if (textbox_BillMonth.Enabled && textbox_BillMonth.ReadOnly == false)
            {
                //20190613150225 furukawa st ////////////////////////
                //年度チェック令和対応
                
                if (int.TryParse(textbox_BillMonth.Text, out billmonth) == false || billmonth < 104 || billmonth % 100 < 1 || billmonth % 100 > 12)
                        //if (int.TryParse(textbox_BillMonth.Text, out billmonth) == false || billmonth < 2000 || billmonth % 100 < 1 || billmonth % 100 > 12)
                //20190613150225 furukawa ed ////////////////////////

                {
                    textbox_BillMonth.Select();
                    textbox_BillMonth.SelectAll();
                    throw new Exception("請求月を入力して下さい");
                }

                // 請求月の更新 （グループごと 一括変更する・データ出力してなかったことにする。（要再出力））
                SQLClass.ExecuteNonQuery("UPDATE t_data SET billmonth = :billmonth, notoutput = 1 WHERE gid = :gid",
                                         "billmonth", typeof(int), billmonth,
                                         "gid", typeof(int), m_GID);
            }



            // 診療月のチェック
            int medimonth;
            if (textbox_MediMonth1.Text == "----") 
            {
                medimonth = 9999;                       // ---- 続紙    9999
                Hiho1.Enabled = false;                  // 被保険者番号
            }
            else if (textbox_MediMonth1.Text == "****")  
            {
                medimonth = 0;                          // **** エラー  0000
                Hiho1.Enabled = false;                  // 被保険者番号
            }

            //20190613150337 furukawa st ////////////////////////
            //年チェック令和対応
            
            else if (int.TryParse(textbox_MediMonth1.Text, out medimonth) == false || medimonth < 104 || medimonth % 100 < 1 || medimonth % 100 > 12)
                    //else if (int.TryParse(textbox_MediMonth1.Text, out medimonth) == false || medimonth < 2000 || medimonth % 100 < 1 || medimonth % 100 > 12)
            //20190613150337 furukawa ed ////////////////////////

            {
                textbox_MediMonth1.Select();
                throw new Exception("診療月を入力して下さい");
            }


            int insuredno = -1;
            int typecode = -1;
            if (medimonth != 0 && medimonth != 9999)
            {
                // 区分のチェック                
                if (textBoxTypecode1.Text == "1" || textBoxTypecode1.Text == "2")
                {
                    typecode = int.Parse(textBoxTypecode1.Text);
                }
                else
                {
                    textBoxTypecode1.Select();
                    textBoxTypecode1.SelectAll();
                    throw new Exception("区分を入力して下さい。（1:鍼灸, 2:あんまマッサージ）");
                }


                // 被保険者番号のチェック
                if (Hiho1.Text.Trim().Length < Hiho1.MaxLength || int.TryParse(Hiho1.Text.Trim(), out insuredno) == false || insuredno < 0)
                {
                    Hiho1.Enabled = true;
                    Hiho1.Select();
                    throw new Exception("被保険者番号を入力して下さい");
                }
                
                // 支払先名のチェック
                if (textBoxBillname.Text.Trim() == "")
                {
                    textBoxBillname.Enabled = true;
                    textBoxBillname.Select();
                    throw new Exception("支払先名を入力して下さい");
                }

                // 施術院名のチェック
                if (textBoxHosname1.Text.Trim()=="")
                {
                    textBoxHosname1.Enabled = true;
                    textBoxHosname1.Select();
                    throw new Exception("施術院名を入力して下さい");
                }
            }
            
            // ベリファイチェック
            bool IsVerify = m_ISVERIFY;
            textBoxTypecode2.Visible = (IsVerify && (textBoxTypecode2.Text.Trim() != textBoxTypecode1.Text.Trim()));            // 区分のチェック
            textbox_MediMonth2.Visible = (IsVerify && (textbox_MediMonth2.Text.Trim() != textbox_MediMonth1.Text.Trim()));      // 診療年月のチェック
            IsVerify = IsVerify && medimonth != 0 && medimonth != 9999;
            Hiho2.Visible = (IsVerify && (Hiho2.Text.Trim() != Hiho1.Text.Trim()));                                             // 被保険者番号のチェック
            textBoxInsuredName2.Visible = (IsVerify && (textBoxInsuredName2.Text.Trim() != textBoxInsuredName1.Text.Trim()));   // 氏名のチェック
            textBoxHosname2.Visible = (IsVerify && (textBoxHosname2.Text.Trim() != textBoxHosname1.Text.Trim()));               // 施術院名のチェック

            if (textBoxTypecode2.Visible)
            {
                textBoxTypecode1.Enabled = true;
                textBoxTypecode1.Select();
                throw new Exception("入力チェックエラー");
            }
            
            if (textbox_MediMonth2.Visible)
            {
                textbox_MediMonth1.Enabled = true;
                textbox_MediMonth1.Select();
                throw new Exception("入力チェックエラー");
            }

            if (Hiho2.Visible)
            {
                Hiho1.Enabled = true;
                Hiho1.Select();
                throw new Exception("入力チェックエラー");
            }

            if (textBoxInsuredName2.Visible)
            {
                textBoxInsuredName1.Enabled = true;
                textBoxInsuredName1.Select();
                throw new Exception("入力チェックエラー");
            }

            if (textBoxHosname2.Visible)
            {
                textBoxHosname1.Enabled = true;
                textBoxHosname1.Select();
                throw new Exception("入力チェックエラー");
            }



            // UPDATE t_data SET 列名 = 値, 列名 = 値, …　WHEER fname = ファイル名　コマンドの設定
            var p = new List<object>();                                                         // パラメータ一覧
            p.Add("medimonth"); p.Add(typeof(int)); p.Add(medimonth);                           // 診療年月

            if (medimonth != 0 && medimonth != 9999)
            {
                p.Add("insuredno"); p.Add(typeof(int)); p.Add(insuredno);                               // 被保険者番号
                p.Add("insuredname"); p.Add(typeof(string)); p.Add(textBoxInsuredName1.Text.Trim());    // 患者名
                p.Add("billname"); p.Add(typeof(string)); p.Add(textBoxBillname.Text.Trim());           // 支払い先
                p.Add("hosname"); p.Add(typeof(string)); p.Add(textBoxHosname1.Text.Trim());            // 接術機関名
                p.Add("typecode"); p.Add(typeof(int)); p.Add(typecode);                                 // 区分
            }
            else
            {
                p.Add("insuredno"); p.Add(typeof(int)); p.Add(DBNull.Value);                            // 被保険者番号
                p.Add("insuredname"); p.Add(typeof(string)); p.Add(DBNull.Value);                       // 患者名
                p.Add("hosname"); p.Add(typeof(string)); p.Add(DBNull.Value);                           // 接術機関名
                p.Add("typecode"); p.Add(typeof(int)); p.Add(DBNull.Value);                             // 区分
            }

            p.Add("fname"); p.Add(typeof(int)); p.Add(m_FNAME);

            p.Add("notinput");  p.Add(typeof(int)); p.Add(0);
            p.Add("notverify");  p.Add(typeof(int)); p.Add(m_ISVERIFY ? 0 : 1);

            // 変更チェック
            bool chkall = false;
            bool chkbillname = false;
            using (var r = SQLClass.ExecuteReader("SELECT * FROM t_data WHERE fname = :fname", "fname", typeof(int), m_FNAME))
            {
                if (r.Read())
                {
                    if(r["billname"].ToString() != textBoxBillname.Text.Trim()) chkbillname = true;
                    for (int i = 0; i < p.Count; i += 3)
                    {
                        if (r[p[i].ToString()].ToString() != p[i + 2].ToString())
                        {
                            chkall = true;
                            break;
                        }
                    }
                }
            }

            // 支払先の変更チェック
            using (var r = SQLClass.ExecuteReader("SELECT * FROM t_data WHERE fname = :fname", "fname", typeof(int), m_FNAME))

            if (chkall)
            {
                p.Add("notoutput"); p.Add(typeof(int)); p.Add(1);                               // 出力日なし
                var t = new List<string>();
                for (int i = 0; i < p.Count; i += 3) t.Add(p[i].ToString() + " = :" + p[i].ToString());
                string query = "UPDATE t_data SET " + string.Join(",", t.ToArray()) + " WHERE fname = :fname;";
                if(chkbillname)
                {
                    query += "UPDATE t_data SET billname = :billname WHERE gid = :gid AND notinput = 1 AND notverify = 1";
                    p.Add("gid"); p.Add(typeof(int)); p.Add(m_GID);
                }

                SQLClass.ExecuteNonQuery(query , p.ToArray());
            }
        }
        
        private void buttonChangeBillname_Click(object sender, EventArgs e)
        {
            if (textBoxBillname.Enabled)
            {
                textBoxBillname.Select();
            }
            else if (MessageBox.Show("支払先を変更しますか？", "支払先変更", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                textBoxBillname.Enabled = true;
                textBoxBillname.Select();
            }
        }

        private void buttonChangeBillmonth_Click(object sender, EventArgs e)
        {
            if (textbox_BillMonth.Enabled)
            {
                textbox_BillMonth.Select();
            }
            else if (MessageBox.Show("請求月を変更しますか？", "請求月変更", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                textbox_BillMonth.Enabled = true;
                textbox_BillMonth.Select();
            }
        }

        /// <summary>戻る・次へボタンの同期用 </summary>
        bool buttonEnable = true;

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            if (buttonEnable == false)
            {
                System.Diagnostics.Debug.WriteLine("buttonNext_Click ロック中");
                return;
            }
            buttonEnable = false;

            try
            {
                // 上書きする。
                DBWrite();
            }
            catch (Exception ex)
            {
                if (mStatus == eStatus.チェック済 || mStatus == eStatus.入力済)
                {
                    MessageBox.Show(ex.Message);
                    buttonEnable = true;
                    return;
                }
            }
            if (ShowPrevData() == false)
            {
                mStatus = (m_ISVERIFY ? eStatus.チェック済 : eStatus.入力済);
            }


            buttonEnable = true;
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            if (buttonEnable == false)
            {
                System.Diagnostics.Debug.WriteLine("buttonNext_Click ロック中");
                return;
            }
            buttonEnable = false;

            try
            {
                // 上書きする。
                DBWrite();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                buttonEnable = true;
                return;
            }
            if (ShowNextData() == false)
            {
                mStatus = (m_ISVERIFY ? eStatus.チェック済 : eStatus.入力済);
                MessageBox.Show((m_ISVERIFY ? "チェック完了" : "入力完了"));
            }

            buttonEnable = true;
        }


        private void buttonCCW_Click(object sender, EventArgs e)
        {
            RotateImage(RotateFlipType.Rotate270FlipNone);
        }

        private void buttonCW_Click(object sender, EventArgs e)
        {
            RotateImage(RotateFlipType.Rotate90FlipNone);
        }

        void RotateImage(RotateFlipType r)
        {
            try 
	        {
                SQLClass.ExecuteNonQuery("UPDATE t_data SET notoutput = 1, notimgout = 1 WHERE fname = :fname", "fname", typeof(int), m_FNAME);
                using (var fs = File.OpenRead(pictureBox.ImageLocation))
		        using(var img = Image.FromStream(fs))
                {
                    fs.Close();
                    img.RotateFlip(r);
                    img.Save(pictureBox.ImageLocation, img.RawFormat);
                }

                pictureBox.ImageLocation = C_GlobalData.GetFilePath(Convert.ToInt32(m_FNAME));
	        }
	        catch (Exception)
	        {
	        }
        }

        
        /// <summary> 診療年月の変更時の処理 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textbox_MediMonth1_TextChanged(object sender, EventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox.Text == "----" || textbox.Text == "****")
            {
                textBoxTypecode1.Enabled = false;
                Hiho1.Enabled = false;
                textBoxInsuredName1.Enabled = false;
                buttonChangeBillname.Enabled = false;
                textBoxBillname.Enabled = false;
                textBoxHosname1.Enabled = false;
            }
            else
            {
                textBoxTypecode1.Enabled = true;
                Hiho1.Enabled = true;
                textBoxInsuredName1.Enabled = true;
                buttonChangeBillname.Enabled = true;
                textBoxHosname1.Enabled = true;
            }
        }

        /// <summary> 診療年月のテキスト入力時</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textbox_MediMonth2_TextChanged(object sender, EventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox.Text == "----" || textbox.Text == "****")
            {
                textBoxTypecode2.Enabled = false;
                Hiho2.Enabled = false;
                textBoxInsuredName2.Enabled = false;
                textBoxHosname2.Enabled = false;
            }
            else
            {
                textBoxTypecode2.Enabled = true;
                Hiho2.Enabled = true;
                textBoxInsuredName2.Enabled = true;
                textBoxHosname2.Enabled = true;
            }
        }
        
        private void SelectionTextbox_TextChanged(object sender, EventArgs e)
        {            
            C_GlobalData.SelectionTextboxChanged(sender as TextBox);
        }
        
        private void textbox_KeyDown(object sender, KeyEventArgs e)
        {
            //Enterキー入力 かつ imgが入力途中では無いとき
            if (e.KeyCode == Keys.Enter && ImmGetOpenStatus((sender as Control).Handle) == 0)
            {
                //あたかもTabキーが押されたかのようにする
                //Shiftが押されている時は前のコントロールのフォーカスを移動
                this.ProcessTabKey(!e.Shift);
                e.Handled = true;
            }
        }

        private void SelectionTextbox_Leave(object sender, EventArgs e)
        {
            labelSelection.Visible = false;
        }

        /// <summary> 氏名欄へのフォーカス移動時 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxInsuredName_Enter(object sender, EventArgs e)
        {
            if (textBox_EnterEnable == false) return;
            textBox_EnterEnable = false;

            int insuredno;
            if (int.TryParse(Hiho1.Text.Trim(), out insuredno) == false)
            {
                textBox_EnterEnable = true;
                return;
            }

            var t = sender as TextBox;
            t.AutoCompleteCustomSource.Clear();
            C_GlobalData.SelectionTextboxPreviuew(t,
                                                  labelSelection,
                                                  "SELECT insuredname FROM t_data WHERE insuredno = :insuredno GROUP BY insuredname ORDER BY MIN(fname)",
                                                  "insuredno", typeof(int), insuredno);

            textBox_EnterEnable = true;
        }
        
        private void textBoxBillname_Enter(object sender, EventArgs e)
        {
            if (textBox_EnterEnable == false) return;
            textBox_EnterEnable = false;


            var t = sender as TextBox;
            t.AutoCompleteCustomSource.Clear();
            C_GlobalData.SelectionTextboxPreviuew(t,
                                                  labelSelection,
                                                  "SELECT billname FROM t_data GROUP BY billname ORDER BY MIN(fname)");


            textBox_EnterEnable = true;
        }


        bool textBox_EnterEnable = true;
        private void textBoxHosname_Enter(object sender, EventArgs e)
        {
            if (textBox_EnterEnable == false) return;
            textBox_EnterEnable = false;

            var textbox = sender as TextBox;
            textbox.AutoCompleteCustomSource.Clear();
            textbox.AutoCompleteCustomSource.Add(textBoxBillname.Text.Trim());

            C_GlobalData.SelectionTextboxPreviuew(textbox,labelSelection,
                                                  "SELECT hosname FROM t_data WHERE billname = :billname AND not (hosname = :billname) GROUP BY hosname ORDER BY MIN(fname)",
                                                  "billname", typeof(string), textBoxBillname.Text.Trim());


            textBox_EnterEnable = true;
        }

    }
}
