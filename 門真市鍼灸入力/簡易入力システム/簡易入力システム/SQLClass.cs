﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.Common;
using System.IO;

#if POSTGRESQL
using Npgsql;
#elif SQLIGHT
using System.Data.SQLite;
#endif

namespace WindowsFormsApplication1
{
    /// <summary> SQLコネクションクラス （コンパイルスイッチにて、PostgreSQL と SQLightを切り替える）</summary>
    class SQLClass
    {
#if POSTGRESQL
        static NpgsqlConnection con = null;
#elif SQLIGHT
        const string FILENAME = "ReceiptInput.db";
        static SQLiteConnection con = null;
#endif

        /// <summary> 接続OPEN・再接続        /// </summary>
        public static void Open()
        {
            Close();

            try
            {
#if POSTGRESQL
                if (con == null) con = new NpgsqlConnection(C_GlobalData.ConnectText());
#elif SQLIGHT
                string path = C_GlobalData.GetPictureDir() + "\\" + FILENAME;

                if (File.Exists(path) == false) throw new Exception(path + "\r\nファイルがありません。");
                if (con == null) con = new SQLiteConnection("Data Source=" + path);
#endif
                con.Open();
                if (!ConState()) throw new Exception("サーバーとの接続で問題が発生しました。サーバーの状態、もしくは設定を確認してください。");
            }
            catch (Exception ex)
            {
                con = null;
                MessageBox.Show(ex.Message, "DB接続エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static void Close()
        {
            if (con != null)
            {
                con.Close();
                con.Dispose();
                con = null;
            }
        }



        /// <summary> テキストデータを1件だけ読み込む </summary>
        /// <param name="commandText"> コマンドテキスト </param>
        /// <param name="commandParams"> パラメータ （パラメータ名, Type, 値, パラメータ名, Type, 値,  … ） </param>
        /// <returns> 一覧 </returns>
        public static string readStringData(string commandText, params  object[] commandParams)
        {
            return readData(commandText, commandParams).ToString();
        }
        
        /// <summary> 数値データを1件だけ読み込む </summary>
        /// <param name="commandText"> コマンドテキスト </param>
        /// <param name="commandParams"> パラメータ （パラメータ名, Type, 値, パラメータ名, Type, 値,  … ） </param>
        /// <returns> 一覧 </returns>
        public static long readNumelicData(string commandText, params  object[] commandParams)
        {
            return Convert.ToInt64(readData(commandText, commandParams));
        }
        
        /// <summary> データを1件だけ読み込む </summary>
        /// <param name="commandText"> コマンドテキスト </param>
        /// <param name="commandParams"> パラメータ （パラメータ名, Type, 値, パラメータ名, Type, 値,  … ） </param>
        /// <returns> 一覧 </returns>
        public static object readData(string commandText, params  object[] commandParams)
        {
            var cmd = GetDbCommand(commandText, commandParams);

            object outData;
            using (DbDataReader reader = cmd.ExecuteReader())
            {
                reader.Read();
                outData = reader[0];
                reader.Close();
            }
            return outData;     
        }


        /// <summary> テキストデータを複数件読み込む </summary>
        /// <param name="commandText">コマンド</param>
        /// <param name="args">コマンドに付加するパラメータ {0} {1} {2] で管理</param>
        /// <returns>出力先のデータのリスト</returns>
        public static string[] readStringDataList(string commandText, params object[] commandParams)
        {
            object[] o = readDataList(commandText, commandParams);
            string[] n = new string[o.Length];
            for (int i = 0; i < o.Length; i++) n[i] = o[i].ToString();
            return n;
        }


        /// <summary> データを複数件読み込む </summary>
        /// <param name="commandText"> コマンドテキスト </param>
        /// <param name="commandParams"> パラメータ （パラメータ名, Type, 値, パラメータ名, Type, 値,  … ） </param>
        /// <returns>出力先のデータのリスト</returns>
        public static object[] readDataList(string commandText, params object[] commandParams)
        {
            var outData = new List<object>();
            var c = GetDbCommand(commandText, commandParams);
            using (var r = c.ExecuteReader())
            {
                while (r.Read())
                {
                    for (int i = 0; i < r.FieldCount; i++)
                    {
                        outData.Add(r[i]);
                    }
                }
            }

            return outData.ToArray();
        }





        static Dictionary<string, DbCommand> m_CommandList = new Dictionary<string, DbCommand>();

        /// <summary> SQLコマンドを作成する。 </summary>
        /// <param name="commandText"> コマンドテキスト </param>
        /// <param name="commandParams"> パラメータ （パラメータ名, Type, 値, パラメータ名, Type, 値,  … ） </param>
        /// <returns> SQLコマンド </returns>
        public static DbCommand GetDbCommand(string commandText, params object[] commandParams)
        {
            DbCommand cmd;
            string name;
            Type type;
            object value;


            if (m_CommandList.ContainsKey(commandText) == false)
            {
#if POSTGRESQL 
                cmd = new NpgsqlCommand(commandText, con);
#elif SQLIGHT
                cmd =  new SQLiteCommand(commandText, con);
#endif
                for (int i = 0; i < commandParams.Length; i += 3)
                {
                    name = commandParams[i].ToString();
                    type = (Type)commandParams[i + 1];
                    AddParam(cmd, name, type);
                }
                
                m_CommandList[commandText] = cmd;
            }
            
            cmd = m_CommandList[commandText];
            for (int i = 0; i < commandParams.Length; i += 3)
            {
                name = commandParams[i].ToString();
                value = commandParams[i + 2];
                cmd.Parameters[name].Value = value;
            }
            return cmd;
        }

        /// <summary> SQL読込。 </summary>
        /// <param name="commandText"> コマンドテキスト </param>
        /// <param name="commandParams"> パラメータ （パラメータ名, Type, 値, パラメータ名, Type, 値,  … ） </param>
        /// <returns> DbDataReader </returns>
        public static DbDataReader ExecuteReader(string commandText, params object[] commandParams)
        {
            var cmd = GetDbCommand(commandText, commandParams);
            return cmd.ExecuteReader();
        }

        /// <summary> SQL実行。 </summary>
        /// <param name="commandText"> コマンドテキスト </param>
        /// <param name="commandParams"> パラメータ （パラメータ名, Type, 値, パラメータ名, Type, 値,  … ） </param>
        /// <returns> 変更した数 </returns>
        public static int ExecuteNonQuery(string commandText, params object[] commandParams)
        {
            var cmd = GetDbCommand(commandText, commandParams);
            return cmd.ExecuteNonQuery();
        }

        

        /// <summary> DB更新 （INSERT or UPDATE） 処理  (関数内では、Transuctionや例外処理はしない！) </summary>
        /// <param name="cnt">SELECT COUNT(*) コマンド （INSERT or UPDATEを特定するために使用する！）</param>
        /// <param name="ins">INSERTコマンド</param>
        /// <param name="upd">UPDATEコマンド</param>
        /// <param name="commandParams"> パラメータ （パラメータ名, Type, 値, パラメータ名, Type, 値,  … ） </param>
        /// <returns> 変更した数 </returns>
        public static int InsertUpdate(string cnt, string ins, string upd, params object[] commandParams)
        {
            string ins_upd;         // Insert or Updateコマンド
            using (var r = ExecuteReader(cnt, commandParams))                   // 重複チェック
            {
                if (r.Read() == false) ins_upd = ins;                           // 重複レコードなし -> INS
                else if (r[0] == null) ins_upd = ins;                           // 重複レコードなし -> INS
                else if (r[0].GetType() == typeof(DBNull)) ins_upd = ins;       // 重複レコードなし -> INS
                else if (Convert.ToInt32(r[0]) <= 0) ins_upd = ins;             // 重複レコードなし -> INS
                else ins_upd = upd;                                             // 重複レコードあり -> UPDATE
            }

            return ExecuteNonQuery(ins_upd, commandParams);                     // Insert or Updateコマンドの実行
        }
        
        static Dictionary<string, DbDataAdapter> m_DbDataAdapterList = new Dictionary<string, DbDataAdapter>();

        /// <summary> DbDataAdapterの取得 </summary>
        /// <param name="commandText"> コマンドテキスト </param>
        /// <param name="commandParams"> パラメータ （パラメータ名, Type, 値, パラメータ名, Type, 値,  … ） </param>
        /// <returns> </returns>
        public static DbDataAdapter GetDbDataAdapter(string commandText, params  object[] commandParams)
        {
            DbDataAdapter da;
            string name;
            Type type;
            object value;

            if (m_DbDataAdapterList.ContainsKey(commandText) == false)
            {
#if POSTGRESQL
                da = new NpgsqlDataAdapter(commandText, con);
#elif SQLIGHT
                da =  new SQLiteDataAdapter(commandText, con);
#endif
                for (int i = 0; i < commandParams.Length; i += 3)
                {
                    name = commandParams[i].ToString();
                    type = (Type)commandParams[i + 1];
                    AddParam(da.SelectCommand, name, type);
                }

                m_DbDataAdapterList[commandText] = da;
            }

            da = m_DbDataAdapterList[commandText];
            for (int i = 0; i < commandParams.Length; i += 3)
            {
                name = commandParams[i].ToString();
                value = commandParams[i + 2];
                da.SelectCommand.Parameters[name].Value = value;
            }
            return da;
        }

        /// <summary> DataTableへの読込 </summary>
        /// <param name="commandText"> コマンドテキスト </param>
        /// <param name="commandParams"> パラメータ （パラメータ名, Type, 値, パラメータ名, Type, 値,  … ） </param>
        /// <returns> </returns>
        public static DataTable CreateDbDataTable(string commandText, params  object[] commandParams)
        {
            var dt = new DataTable();
            var da = GetDbDataAdapter(commandText, commandParams);
            da.Fill(dt);
            return dt;
        }


        /// <summary> 一覧への読込 </summary>
        /// <param name="commandText"> コマンドテキスト </param>
        /// <param name="commandParams"> パラメータ （パラメータ名, Type, 値, パラメータ名, Type, 値,  … ） </param>
        /// <returns> 一覧 </returns>
        public static HashSet<string> Read_2_HashSet(string commandText, params object[] commandParams)
        {
            var hs = new HashSet<string>();
            var c = GetDbCommand(commandText, commandParams);
            using (var r = c.ExecuteReader())
            {
                while (r.Read())
                {
                    for (int i = 0; i < r.FieldCount; i++) hs.Add(r[i].ToString());
                }
            }
            return hs;
        }

        public static void AddParam(DbCommand command, string name, Type type)
        {
#if POSTGRESQL
            var c = (NpgsqlCommand)command;
            if(type == typeof(int)) c.Parameters.Add(name, NpgsqlTypes.NpgsqlDbType.Integer);
            else if(type == typeof(long)) c.Parameters.Add(name, NpgsqlTypes.NpgsqlDbType.Numeric);
            else if (type == typeof(string)) c.Parameters.Add(name, NpgsqlTypes.NpgsqlDbType.Text);            
#elif SQLIGHT
            var c = (SQLiteCommand)command;
            if(type == typeof(int)) c.Parameters.Add(name, DbType.Int32);
            else if (type == typeof(long)) c.Parameters.Add(name, DbType.Int64);
            else if (type == typeof(string)) c.Parameters.Add(name, DbType.String);

#endif
        }
        

        /// <summary> 接続の状態 </summary>
        public static bool ConState()
        {
            if (con == null) return false;
            return (con.State == System.Data.ConnectionState.Open);
        }


        /// <summary>
        /// トランザクションを開始します
        /// </summary>
        /// <returns></returns>
        public static DbTransaction BeginTran()
        {
            return con.BeginTransaction();
        }
        
        /// <summary> テーブル名一覧を取得する</summary>
        /// <returns>テーブル名一覧</returns>
        public static string[] TableNameList()
        {
#if POSTGRESQL
            return readStringDataList("select relname as TABLE_NAME from pg_stat_user_tables");
#elif SQLIGHT
            return readStringDataList("select name from sqlite_master where type = 'table'");
#endif
        }

        /// <summary> 数字型コード の 先頭文字 → 数値の最小値（以上）を取得する </summary>
        /// <param name="text">先頭文字      例： 012</param>
        /// <param name="keta">コードの桁数  例： 8桁</param>
        /// <returns>コードの最小値  例：01200000以上</returns>
        static public long Like_2_min(string text, int keta)
        {
            text = text.Trim();                                         // 空白を除去
            if (text.Length > keta) text = text.Remove(keta);           // 余った桁数を削除
            if (text.Length < keta) text = text.PadRight(keta, '0');    // 足りない桁数は０埋め

            string tmp = "";                                            // 数字以外は０に置き換える
            foreach (var c in text.Trim())
            {

                if (c >= '0' && c <= '9') tmp += c;
                else if (c >= '０' && c <= '９') tmp += c - '０' + '0';
                else tmp += '0';
            }

            return long.Parse(tmp);
        }

        /// <summary> 数字型コード の 先頭文字 → 数値の最大値（以下）を取得する </summary>
        /// <param name="text">先頭文字      例： 012</param>
        /// <param name="keta">コードの桁数  例： 8桁</param>
        /// <returns>コードの最小値  例：01299999以下</returns>
        static public long Like_2_max(string text, int keta)
        {
            text = text.Trim();                                         // 空白を除去
            if (text.Length > keta) text = text.Remove(keta);           // 余った桁数を削除
            if (text.Length < keta) text = text.PadRight(keta, '9');    // 足りない桁数は9埋め

            string tmp = "";                                            // 数字以外は９に置き換える
            foreach (var c in text.Trim())
            {

                if (c >= '0' && c <= '9') tmp += c;
                else if (c >= '０' && c <= '９') tmp += c - '０' + '0';
                else tmp += '9';
            }

            return long.Parse(tmp);
        }
    }
}
