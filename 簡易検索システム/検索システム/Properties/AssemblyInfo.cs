﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// アセンブリに関する一般情報は以下の属性セットをとおして制御されます。
// アセンブリに関連付けられている情報を変更するには、
// これらの属性値を変更してください。

[assembly: AssemblyTitle("鍼灸検索システム")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("メディブレーン")]

[assembly: AssemblyProduct("鍼灸検索システム"

// 役所名 （K門真市、W和歌山広域）
#if KADOMA_CITY
 + " K"
#elif WAKA_KOIKI
+ " W"
#else
#error　コンパイルエラー！ コンパイルスイッチにて、役所名を指定してください
#endif

// エンジン （P PostgreSql、L SQLight）
#if POSTGRESQL
+ " P"
#elif SQLIGHT
 + " L"
#else
#error　コンパイルエラー！ コンパイルスイッチにて、SQLのエンジンを指定してください
#endif

// D Debug
#if DEBUG
 + " D"
#endif
)]


[assembly: AssemblyCopyright("Copyright © メディブレーン 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// ComVisible を false に設定すると、その型はこのアセンブリ内で COM コンポーネントから 
// 参照不可能になります。COM からこのアセンブリ内の型にアクセスする場合は、
// その型の ComVisible 属性を true に設定してください。
[assembly: ComVisible(false)]

// 次の GUID は、このプロジェクトが COM に公開される場合の、typelib の ID です
[assembly: Guid("5838518e-2690-4abc-a4a8-d9927a03d0fc")]

// アセンブリのバージョン情報は、以下の 4 つの値で構成されています:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// すべての値を指定するか、下のように '*' を使ってビルドおよびリビジョン番号を 
// 既定値にすることができます:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.1.0.1")]
[assembly: AssemblyFileVersion("2.1.0.1")]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("TestProject1検索システム")]