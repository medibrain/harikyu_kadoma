﻿namespace ReceSeek_Harikyu
{
    partial class FormMain
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.dataGrid = new System.Windows.Forms.DataGridView();
            this.seek_Panel = new System.Windows.Forms.Panel();
            this.checkBoxAll = new System.Windows.Forms.CheckBox();
            this.SettingButton = new System.Windows.Forms.Button();
            this.printPreview2 = new System.Windows.Forms.Button();
            this.printPreview1 = new System.Windows.Forms.Button();
            this.printSettingButton = new System.Windows.Forms.Button();
            this.label_Count = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonCLS = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.billname = new System.Windows.Forms.ComboBox();
            this.hosname = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.seek_BillDateMax = new System.Windows.Forms.TextBox();
            this.seek_BillDateMin = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.seek_MediDateMax = new System.Windows.Forms.TextBox();
            this.seek_MediDateMin = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.insuredname = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.insuredno = new System.Windows.Forms.TextBox();
            this.button_seek = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.printDocument = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkBox_Margin = new System.Windows.Forms.CheckBox();
            this.print1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button_nextPage = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button_prevPage = new System.Windows.Forms.Button();
            this.label_MaxPage = new System.Windows.Forms.Label();
            this.text_PageNo = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel_PictureBox = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
            this.seek_Panel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel_PictureBox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGrid
            // 
            this.dataGrid.AllowUserToAddRows = false;
            this.dataGrid.AllowUserToDeleteRows = false;
            this.dataGrid.AllowUserToOrderColumns = true;
            this.dataGrid.AllowUserToResizeRows = false;
            this.dataGrid.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGrid.Location = new System.Drawing.Point(0, 154);
            this.dataGrid.MultiSelect = false;
            this.dataGrid.Name = "dataGrid";
            this.dataGrid.RowHeadersVisible = false;
            this.dataGrid.RowTemplate.Height = 21;
            this.dataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGrid.Size = new System.Drawing.Size(526, 376);
            this.dataGrid.TabIndex = 2;
            this.dataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGrid_DataError);
            // 
            // seek_Panel
            // 
            this.seek_Panel.Controls.Add(this.checkBoxAll);
            this.seek_Panel.Controls.Add(this.SettingButton);
            this.seek_Panel.Controls.Add(this.printPreview2);
            this.seek_Panel.Controls.Add(this.printPreview1);
            this.seek_Panel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.seek_Panel.Location = new System.Drawing.Point(0, 530);
            this.seek_Panel.Name = "seek_Panel";
            this.seek_Panel.Size = new System.Drawing.Size(526, 32);
            this.seek_Panel.TabIndex = 0;
            // 
            // checkBoxAll
            // 
            this.checkBoxAll.AutoSize = true;
            this.checkBoxAll.Location = new System.Drawing.Point(10, 8);
            this.checkBoxAll.Name = "checkBoxAll";
            this.checkBoxAll.Size = new System.Drawing.Size(60, 16);
            this.checkBoxAll.TabIndex = 7;
            this.checkBoxAll.Text = "全選択";
            this.checkBoxAll.UseVisualStyleBackColor = true;
            this.checkBoxAll.CheckedChanged += new System.EventHandler(this.checkBoxAll_CheckedChanged);
            // 
            // SettingButton
            // 
            this.SettingButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SettingButton.Location = new System.Drawing.Point(372, 4);
            this.SettingButton.Name = "SettingButton";
            this.SettingButton.Size = new System.Drawing.Size(105, 23);
            this.SettingButton.TabIndex = 3;
            this.SettingButton.Text = "画像CD読み込み";
            this.SettingButton.UseVisualStyleBackColor = true;
            this.SettingButton.Click += new System.EventHandler(this.settingButton_Click);
            // 
            // printPreview2
            // 
            this.printPreview2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.printPreview2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.printPreview2.Location = new System.Drawing.Point(71, 3);
            this.printPreview2.Name = "printPreview2";
            this.printPreview2.Size = new System.Drawing.Size(101, 23);
            this.printPreview2.TabIndex = 5;
            this.printPreview2.Text = "ﾌﾟﾚﾋﾞｭｰ（選択分）";
            this.printPreview2.UseVisualStyleBackColor = true;
            this.printPreview2.Click += new System.EventHandler(this.printPreview2_Click);
            // 
            // printPreview1
            // 
            this.printPreview1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.printPreview1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.printPreview1.Location = new System.Drawing.Point(178, 4);
            this.printPreview1.Name = "printPreview1";
            this.printPreview1.Size = new System.Drawing.Size(114, 23);
            this.printPreview1.TabIndex = 4;
            this.printPreview1.Text = "ﾌﾟﾚﾋﾞｭｰ（1件）";
            this.printPreview1.UseVisualStyleBackColor = true;
            this.printPreview1.Click += new System.EventHandler(this.printPreview1_Click);
            // 
            // printSettingButton
            // 
            this.printSettingButton.Location = new System.Drawing.Point(4, 4);
            this.printSettingButton.Name = "printSettingButton";
            this.printSettingButton.Size = new System.Drawing.Size(86, 23);
            this.printSettingButton.TabIndex = 20;
            this.printSettingButton.Text = "プリンタ選択";
            this.printSettingButton.UseVisualStyleBackColor = true;
            this.printSettingButton.Click += new System.EventHandler(this.printSettingButton_Click);
            // 
            // label_Count
            // 
            this.label_Count.AutoSize = true;
            this.label_Count.ForeColor = System.Drawing.Color.Red;
            this.label_Count.Location = new System.Drawing.Point(312, 136);
            this.label_Count.Name = "label_Count";
            this.label_Count.Size = new System.Drawing.Size(77, 12);
            this.label_Count.TabIndex = 6;
            this.label_Count.Text = "検索結果：0件";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.buttonCLS);
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Controls.Add(this.groupBox5);
            this.panel2.Controls.Add(this.label_Count);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.button_seek);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(526, 154);
            this.panel2.TabIndex = 1;
            // 
            // buttonCLS
            // 
            this.buttonCLS.Location = new System.Drawing.Point(312, 105);
            this.buttonCLS.Name = "buttonCLS";
            this.buttonCLS.Size = new System.Drawing.Size(75, 23);
            this.buttonCLS.TabIndex = 5;
            this.buttonCLS.Text = "条件クリア";
            this.buttonCLS.UseVisualStyleBackColor = true;
            this.buttonCLS.Click += new System.EventHandler(this.buttonCLS_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.billname);
            this.groupBox4.Controls.Add(this.hosname);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Location = new System.Drawing.Point(3, 37);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(517, 64);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            // 
            // billname
            // 
            this.billname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.billname.FormattingEnabled = true;
            this.billname.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.billname.Location = new System.Drawing.Point(86, 11);
            this.billname.Name = "billname";
            this.billname.Size = new System.Drawing.Size(425, 20);
            this.billname.TabIndex = 8;
            this.billname.Validated += new System.EventHandler(this.billname_Validated);
            // 
            // hosname
            // 
            this.hosname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.hosname.FormattingEnabled = true;
            this.hosname.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.hosname.Location = new System.Drawing.Point(86, 37);
            this.hosname.Name = "hosname";
            this.hosname.Size = new System.Drawing.Size(425, 20);
            this.hosname.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 7;
            this.label8.Text = "支払先";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 7;
            this.label6.Text = "施術院名";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.seek_BillDateMax);
            this.groupBox5.Controls.Add(this.seek_BillDateMin);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox5.Location = new System.Drawing.Point(164, 107);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(142, 41);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "請求年月(5桁)";
            // 
            // seek_BillDateMax
            // 
            this.seek_BillDateMax.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.seek_BillDateMax.Location = new System.Drawing.Point(76, 16);
            this.seek_BillDateMax.MaxLength = 5;
            this.seek_BillDateMax.Name = "seek_BillDateMax";
            this.seek_BillDateMax.Size = new System.Drawing.Size(53, 19);
            this.seek_BillDateMax.TabIndex = 1;
            // 
            // seek_BillDateMin
            // 
            this.seek_BillDateMin.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.seek_BillDateMin.Location = new System.Drawing.Point(6, 16);
            this.seek_BillDateMin.MaxLength = 5;
            this.seek_BillDateMin.Name = "seek_BillDateMin";
            this.seek_BillDateMin.Size = new System.Drawing.Size(53, 19);
            this.seek_BillDateMin.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(59, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 2;
            this.label7.Text = "～";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.seek_MediDateMax);
            this.groupBox3.Controls.Add(this.seek_MediDateMin);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox3.Location = new System.Drawing.Point(9, 107);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(138, 41);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "診療年月（5桁）";
            // 
            // seek_MediDateMax
            // 
            this.seek_MediDateMax.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.seek_MediDateMax.Location = new System.Drawing.Point(76, 16);
            this.seek_MediDateMax.MaxLength = 5;
            this.seek_MediDateMax.Name = "seek_MediDateMax";
            this.seek_MediDateMax.Size = new System.Drawing.Size(53, 19);
            this.seek_MediDateMax.TabIndex = 1;
            // 
            // seek_MediDateMin
            // 
            this.seek_MediDateMin.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.seek_MediDateMin.Location = new System.Drawing.Point(6, 16);
            this.seek_MediDateMin.MaxLength = 5;
            this.seek_MediDateMin.Name = "seek_MediDateMin";
            this.seek_MediDateMin.Size = new System.Drawing.Size(53, 19);
            this.seek_MediDateMin.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(59, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "～";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.insuredname);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.insuredno);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(122, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(401, 33);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // insuredname
            // 
            this.insuredname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.insuredname.FormattingEnabled = true;
            this.insuredname.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.insuredname.Location = new System.Drawing.Point(178, 8);
            this.insuredname.Name = "insuredname";
            this.insuredname.Size = new System.Drawing.Size(216, 20);
            this.insuredname.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(149, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "氏名";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "被保険者番号";
            // 
            // insuredno
            // 
            this.insuredno.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.insuredno.Location = new System.Drawing.Point(86, 9);
            this.insuredno.MaxLength = 6;
            this.insuredno.Name = "insuredno";
            this.insuredno.Size = new System.Drawing.Size(57, 19);
            this.insuredno.TabIndex = 0;
            this.insuredno.Validated += new System.EventHandler(this.insuredno_Validated);
            // 
            // button_seek
            // 
            this.button_seek.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button_seek.Location = new System.Drawing.Point(393, 105);
            this.button_seek.Name = "button_seek";
            this.button_seek.Size = new System.Drawing.Size(75, 23);
            this.button_seek.TabIndex = 6;
            this.button_seek.Text = "検索";
            this.button_seek.UseVisualStyleBackColor = true;
            this.button_seek.Click += new System.EventHandler(this.button_seek_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.White;
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(504, 530);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            this.pictureBox.LoadCompleted += new System.ComponentModel.AsyncCompletedEventHandler(this.pictureBox_LoadCompleted);
            this.pictureBox.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // printDocument
            // 
            this.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog
            // 
            this.printPreviewDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog.Document = this.printDocument;
            this.printPreviewDialog.Enabled = true;
            this.printPreviewDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog.Icon")));
            this.printPreviewDialog.Name = "printPreviewDialog";
            this.printPreviewDialog.Visible = false;
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.Controls.Add(this.checkBox_Margin);
            this.panel3.Controls.Add(this.print1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.button_nextPage);
            this.panel3.Controls.Add(this.printSettingButton);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.button_prevPage);
            this.panel3.Controls.Add(this.label_MaxPage);
            this.panel3.Controls.Add(this.text_PageNo);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 530);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(504, 32);
            this.panel3.TabIndex = 9;
            // 
            // checkBox_Margin
            // 
            this.checkBox_Margin.AutoSize = true;
            this.checkBox_Margin.Location = new System.Drawing.Point(177, 7);
            this.checkBox_Margin.Name = "checkBox_Margin";
            this.checkBox_Margin.Size = new System.Drawing.Size(66, 16);
            this.checkBox_Margin.TabIndex = 22;
            this.checkBox_Margin.Text = "余白あり";
            this.checkBox_Margin.UseVisualStyleBackColor = true;
            // 
            // print1
            // 
            this.print1.Location = new System.Drawing.Point(96, 4);
            this.print1.Name = "print1";
            this.print1.Size = new System.Drawing.Size(75, 23);
            this.print1.TabIndex = 21;
            this.print1.Text = "印刷";
            this.print1.UseVisualStyleBackColor = true;
            this.print1.Click += new System.EventHandler(this.print1_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(459, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "/";
            // 
            // button_nextPage
            // 
            this.button_nextPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_nextPage.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button_nextPage.Location = new System.Drawing.Point(481, 6);
            this.button_nextPage.Name = "button_nextPage";
            this.button_nextPage.Size = new System.Drawing.Size(22, 23);
            this.button_nextPage.TabIndex = 0;
            this.button_nextPage.Text = ">";
            this.button_nextPage.UseVisualStyleBackColor = true;
            this.button_nextPage.Click += new System.EventHandler(this.button_nextPage_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(380, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "ページ";
            // 
            // button_prevPage
            // 
            this.button_prevPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_prevPage.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button_prevPage.Location = new System.Drawing.Point(415, 6);
            this.button_prevPage.Name = "button_prevPage";
            this.button_prevPage.Size = new System.Drawing.Size(22, 23);
            this.button_prevPage.TabIndex = 23;
            this.button_prevPage.Text = "<";
            this.button_prevPage.UseVisualStyleBackColor = true;
            this.button_prevPage.Click += new System.EventHandler(this.button_prevPage_Click);
            // 
            // label_MaxPage
            // 
            this.label_MaxPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_MaxPage.AutoSize = true;
            this.label_MaxPage.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label_MaxPage.Location = new System.Drawing.Point(470, 11);
            this.label_MaxPage.Name = "label_MaxPage";
            this.label_MaxPage.Size = new System.Drawing.Size(11, 12);
            this.label_MaxPage.TabIndex = 1;
            this.label_MaxPage.Text = "1";
            // 
            // text_PageNo
            // 
            this.text_PageNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.text_PageNo.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.text_PageNo.Location = new System.Drawing.Point(437, 8);
            this.text_PageNo.MaxLength = 2;
            this.text_PageNo.Name = "text_PageNo";
            this.text_PageNo.Size = new System.Drawing.Size(22, 19);
            this.text_PageNo.TabIndex = 24;
            this.text_PageNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.text_PageNo.TextChanged += new System.EventHandler(this.text_PageNo_TextChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel_PictureBox);
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            this.splitContainer1.Panel1MinSize = 380;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGrid);
            this.splitContainer1.Panel2.Controls.Add(this.seek_Panel);
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Panel2MinSize = 530;
            this.splitContainer1.Size = new System.Drawing.Size(1039, 566);
            this.splitContainer1.SplitterDistance = 508;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 10;
            // 
            // panel_PictureBox
            // 
            this.panel_PictureBox.AutoScroll = true;
            this.panel_PictureBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_PictureBox.Controls.Add(this.pictureBox);
            this.panel_PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_PictureBox.Location = new System.Drawing.Point(0, 0);
            this.panel_PictureBox.Name = "panel_PictureBox";
            this.panel_PictureBox.Size = new System.Drawing.Size(504, 530);
            this.panel_PictureBox.TabIndex = 10;
            this.panel_PictureBox.SizeChanged += new System.EventHandler(this.panel_PictureBox_SizeChanged);
            // 
            // timer1
            // 
            this.timer1.Interval = 200;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(6, 11);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(48, 16);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "鍼灸";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(60, 11);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(53, 16);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "あんま";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBox1);
            this.groupBox2.Controls.Add(this.checkBox2);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(114, 33);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1039, 566);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1000, 600);
            this.Name = "Form1";
            this.Text = "柔整レセプト検索システム";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_CLOSING);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
            this.seek_Panel.ResumeLayout(false);
            this.seek_Panel.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel_PictureBox.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel seek_Panel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.DataGridView dataGrid;
        private System.Windows.Forms.Button button_seek;
        private System.Windows.Forms.TextBox insuredno;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Drawing.Printing.PrintDocument printDocument;
        private System.Windows.Forms.Button printPreview1;
        private System.Windows.Forms.Button printPreview2;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button_nextPage;
        private System.Windows.Forms.Button button_prevPage;
        private System.Windows.Forms.TextBox text_PageNo;
        private System.Windows.Forms.Label label_MaxPage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button SettingButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button printSettingButton;
        private System.Windows.Forms.TextBox seek_BillDateMax;
        private System.Windows.Forms.TextBox seek_BillDateMin;
        private System.Windows.Forms.TextBox seek_MediDateMax;
        private System.Windows.Forms.TextBox seek_MediDateMin;
        private System.Windows.Forms.Label label_Count;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button print1;
        private System.Windows.Forms.Panel panel_PictureBox;
        private System.Windows.Forms.CheckBox checkBox_Margin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox insuredname;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox hosname;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox billname;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBoxAll;
        private System.Windows.Forms.Button buttonCLS;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

