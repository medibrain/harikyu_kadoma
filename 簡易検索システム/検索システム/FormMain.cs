﻿//#define BETA    // βバージョン （所要時間調査）
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Drawing.Printing;
using System.ComponentModel;
using System.Data;
using System.Runtime.InteropServices;

namespace ReceSeek_Harikyu
{
    /// <summary>
    /// 柔整レセプト検索システムのメイン画面のクラス
    /// </summary>
    public partial class FormMain : Form
    {
        [StructLayout(LayoutKind.Sequential)]
        struct MENUITEMINFO
        {
            public uint cbSize;
            public uint fMask;
            public uint fType;
            public uint fState;
            public uint wID;
            public IntPtr hSubMenu;
            public IntPtr hbmpChecked;
            public IntPtr hbmpUnchecked;
            public IntPtr dwItemData;
            public string dwTypeData;
            public uint cch;
            public IntPtr hbmpItem;

            // return the size of the structure
            public static uint sizeOf
            {
                get { return (uint)Marshal.SizeOf(typeof(MENUITEMINFO)); }
            }
        }

        [DllImport("user32.dll")]
        static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("user32.dll")]
        static extern bool InsertMenuItem(IntPtr hMenu, uint uItem, bool fByPosition,
          [In] ref MENUITEMINFO lpmii);

        private const uint MENU_ID_01 = 0x0001;
        private const uint MFT_BITMAP = 0x00000004;
        private const uint MFT_MENUBARBREAK = 0x00000020;
        private const uint MFT_MENUBREAK = 0x00000040;
        private const uint MFT_OWNERDRAW = 0x00000100;
        private const uint MFT_RADIOCHECK = 0x00000200;
        private const uint MFT_RIGHTJUSTIFY = 0x00004000;
        private const uint MFT_RIGHTORDER = 0x000002000;
        private const uint MFT_SEPARATOR = 0x00000800;
        private const uint MFT_STRING = 0x00000000;
        private const uint MIIM_FTYPE = 0x00000100;
        private const uint MIIM_STRING = 0x00000040;
        private const uint MIIM_ID = 0x00000002;
        private const uint WM_SYSCOMMAND = 0x0112;


        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == WM_SYSCOMMAND)
            {
                uint menuid = (uint)(m.WParam.ToInt32() & 0xffff);

                switch (menuid)
                {
                    case MENU_ID_01:
                        using (var f = new FormReadLog())
                        {
                            f.ShowDialog();
                        }
                        break;
                }
            }
        } 

        public FormMain()
        {
            InitializeComponent();

            IntPtr hSysMenu = GetSystemMenu(this.Handle, false);
            MENUITEMINFO item2 = new MENUITEMINFO();
            item2.cbSize = (uint)Marshal.SizeOf(item2);
            item2.fMask = MIIM_STRING | MIIM_ID;
            item2.wID = MENU_ID_01;
            item2.dwTypeData = "システム情報";
            InsertMenuItem(hSysMenu, 6, true, ref item2);
        }

        /// <summary> フォームOPEN時の処理 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
#if WAKA_KOIKI
            Text += "(和歌山広域連合)";
#elif KADOMA_CITY
            Text = "鍼灸レセプト 検索システム";
            insuredno.MaxLength = int.Parse(ReceSeek_Harikyu.Properties.Resources.KADOMA_INSUREDNO_LEN);
#else
#error　コンパイルエラー！ コンパイルスイッチにて、役所名を指定してください
#endif

#if BETA || DEBUG    // βバージョン （所要時間調査）
            DateTime beginTime = DateTime.Now;
            Text += "-βバージョン";
#endif
            // SQLOPEN
            C_GlobalData.sqlOpen();

#if BETA || DEBUG    // βバージョン （所要時間調査）
            MessageBox.Show("sqlOpen\r\n" + C_PostgreSettings.ConnectText + "\r\r" + (DateTime.Now - beginTime).TotalSeconds.ToString() + "秒");
#endif

            UpdateBillList();
            UpdateInsuredNameList();
            UpdateHosList();

            /**  */
            try
            {
                output_2_ListView();


                foreach (DataGridViewColumn col in dataGrid.Columns)
                {
                    col.Visible = false;
                    col.ReadOnly = true;
                }


                int i = 0;
                // ヘッダテキストの変更 (***は表示しない列)
                dataGrid.Columns["chk"].DisplayIndex = i++;
                dataGrid.Columns["chk"].HeaderText = "";
                dataGrid.Columns["chk"].Visible = true;
                dataGrid.Columns["chk"].ReadOnly = false;

                // 1鍼灸 2あんま
                dataGrid.Columns["typename"].DisplayIndex = i++;
                dataGrid.Columns["typename"].HeaderText = "";
                dataGrid.Columns["typename"].Visible = true;


                dataGrid.Columns["billmonth"].DisplayIndex = i++;
                dataGrid.Columns["billmonth"].HeaderText = "請求年月";
                dataGrid.Columns["billmonth"].Visible = true;

                //20190614111517 furukawa st ////////////////////////
                //和暦文字削除
                
                dataGrid.Columns["billmonth"].DefaultCellStyle.Format = "00/00";
                        //dataGrid.Columns["billmonth"].DefaultCellStyle.Format = "平00年00月";
                //20190614111517 furukawa ed ////////////////////////


                dataGrid.Columns["medimonth"].DisplayIndex = i++;
                dataGrid.Columns["medimonth"].HeaderText = "診療年月";
                dataGrid.Columns["medimonth"].Visible = true;

                //20190614111604 furukawa st ////////////////////////
                //和暦文字削除
                
                dataGrid.Columns["medimonth"].DefaultCellStyle.Format = "00/00";
                        //dataGrid.Columns["medimonth"].DefaultCellStyle.Format = "平00年00月";
                //20190614111604 furukawa ed ////////////////////////


                dataGrid.Columns["billname"].DisplayIndex = i++;
                dataGrid.Columns["billname"].HeaderText = "支払先";
                dataGrid.Columns["billname"].Visible = true;

                dataGrid.Columns["hosname"].DisplayIndex = i++;
                dataGrid.Columns["hosname"].HeaderText = "施術院";
                dataGrid.Columns["hosname"].Visible = true;

                dataGrid.Columns["insuredno"].DisplayIndex = i++;
                dataGrid.Columns["insuredno"].HeaderText = "被保番";
                dataGrid.Columns["insuredno"].DefaultCellStyle.Format = "000000";
                dataGrid.Columns["insuredno"].Visible = true;

                dataGrid.Columns["insuredname"].DisplayIndex = i++;
                dataGrid.Columns["insuredname"].HeaderText = "氏名";
                dataGrid.Columns["insuredname"].Visible = true;


                // Resize the DataGridView columns to fit the newly loaded content.
                dataGrid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
            }
            catch (Exception)
            {
            }

            timer1.Start();
        }

        /// <summary>
        /// データビューへのDB出力
        /// </summary>
        /** データビューに出力する  */
        private void output_2_ListView()
        {
            try
            {
                uint n;
                string s;


                var whereList = new List<string>();     // where条件のリスト （where andで区切る前のリスト）

                // 氏名
                if (insuredname.Text.Trim().Length > 0) whereList.Add("insuredname LIKE '" + insuredname.Text.Trim() + "%'");

                // 被保険者番号
                if (insuredno.Text.Trim().Length > 0 && uint.TryParse(insuredno.Text.Trim(), out n) == true) whereList.Add("insuredno = " + n);

                // 施術院名
                if (hosname.Text.Trim().Length > 0) whereList.Add("hosname LIKE '" + hosname.Text.Trim() + "%'");

                // 請求先名
                if (billname.Text.Trim().Length > 0) whereList.Add("billname LIKE '" + billname.Text.Trim() + "%'");

                // 診療年月
                s = minMax(seek_MediDateMin.Text.Trim(), seek_MediDateMax.Text.Trim(), "medimonth");
                if (s.Length > 0) whereList.Add(s);
                else whereList.Add("medimonth > 0 AND medimonth < 9999");

                // 請求年月
                s = minMax(seek_BillDateMin.Text.Trim(), seek_BillDateMax.Text.Trim(), "billmonth");
                if (s.Length > 0) whereList.Add(s);
                else whereList.Add("billmonth > 0 AND billmonth < 9999");


                // 鍼灸・あんま
                if (checkBox1.Checked == true && checkBox2.Checked == false) whereList.Add("typecode = 1");
                else if (checkBox1.Checked == false  && checkBox2.Checked == true) whereList.Add("typecode = 2");

                // クエリ + 検索条件 (where 部分) + 最大出力行数
                string sqlQuery = "SELECT *, Case typecode WHEN 1 THEN '鍼灸' WHEN 2 THEN 'あんま' END AS typename FROM t_data WHERE " + string.Join(" AND ", whereList) + " LIMIT 1001";


                using (var dt = new DataTable())
                using (var da = C_GlobalData.sqlRead2DataAdapter(sqlQuery))
                {
                    dt.Columns.Add("chk", typeof(bool));                                                       // 印刷用のチェックボックスの追加
                    da.Fill(dt);

                    if (dt.Rows.Count > 1000) label_Count.Text = "検索結果\n1001件以上";
                    else label_Count.Text = string.Format("検索結果：{0}件", dt.Rows.Count);


                    if (dt.Rows.Count > 0 && dt.Rows.Count <= 1000) label_Count.ForeColor = Color.Black;
                    else label_Count.ForeColor = Color.Red;

                    dataGrid.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
            }
        }

        string minMax(string strMin, string strMax, string colName)
        {
            int n;
            int min = 0, max = 0;
            if (int.TryParse(strMin, out n) && n > 0) min = (int)(n % 10000);
            if (int.TryParse(strMax, out n) && n > 0) max = (int)(n % 10000);
            if (min > max)
            {
                n = min;
                min = max;
                max = n;
            }

            if (min > 0) return string.Format("{0}>={1} AND {0}<={2}", colName, min, max);
            else if (max > 0) return string.Format("{0}={1}", colName, max);
            else return "";
        }


        /// <summary>
        /// SQLをクローズする
        /// </summary>
        /** フォームCLOSE時の処理 */
        private void Form1_CLOSING(object sender, FormClosingEventArgs e)
        {
            //            backgroundContinueFlag = false;     // バックグラウンドの停止 （フラグで管理）
            C_GlobalData.sqlClose();
        }


        /// <summary>
        /// 検索ボタン処理 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_seek_Click(object sender, EventArgs e)
        {
            //// 診療年月・請求年月・費用額は、入力値をチェックする。
            //if (WarekiModule.checkTextBoxForSeekWareki(seek_MediDateMin) == false) return;  // 診療年月

            output_2_ListView();

            dataGrid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
        }

        
        /// <summary> 現在表示中の画像フルパス名一覧（続紙を含む） </summary>
        List<string> imageList = new List<string>();


        /// <summary> 前回のデータ選択 （変更後が前回と同じ → 選択変更を確定させる）</summary>
        int timer_prevSel = -1;

        /// <summary> 現在のデータ選択 （変更後が現在と同じ → 選択変更は既に確定されているので処理をしない）</summary>
        int timer_nowSel = -1; 

        /// <summary> タイマー処理・DataGridViewの選択変更を管理 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                int fname = (dataGrid.SelectedRows.Count > 0 ? Convert.ToInt32(dataGrid.SelectedRows[0].Cells["fname"].Value) : -1);

                System.Diagnostics.Debug.WriteLine(string.Format("{0}, {1}, {2}", fname, timer_prevSel, timer_nowSel));


                if (timer_prevSel == fname && timer_nowSel != fname)
                {
                    // 前回のタイマー（0.1秒前）と同じデータを選択 → キーが押されていない。
                    // ビューには異なる画像が表示されている        → 表示の更新が必要


                    // 選択を更新する
                    imageList = C_GlobalData.GetZokushiList(fname);                         // リストの更新
                    pictureBox.ImageLocation = (imageList.Count > 0 ? imageList[0] : "");

                    label_MaxPage.Text = imageList.Count.ToString();
                    timer_nowSel = fname;
                }
                else
                {
                    timer_prevSel = fname;
                }
            }
            catch (Exception)
            {
            }
        }

                                
                
        /// <summary>
        /// ページ番号変更イベント
        /// 表示するレセプト画像の変更を行う
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void text_PageNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int n;
                if (int.TryParse(text_PageNo.Text, out n) == false) return;
                else if (n <= 0 || n > imageList.Count) return;
                else pictureBox.ImageLocation = imageList[n - 1];
            }
            catch (Exception)
            {
            }
        }





        /// <summary>
        /// 前ページに移動するボタンの処理
        /// ページ番号を更新する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_prevPage_Click(object sender, EventArgs e)
        {
            // ページNO　現在ページ数 - 1
            uint pageNo = C_GlobalData.string_2_uint32(text_PageNo.Text);
            if (pageNo > 1)
            {
                pageNo = pageNo - 1;
            }
            else
            {
                pageNo = 1;
            }

            try
            {
                uint maxPage = uint.Parse(label_MaxPage.Text);
                if (pageNo > maxPage)
                {
                    pageNo = maxPage;       // ページNO の最大は、ファイルの件数
                }
            }
            catch (Exception)
            {
            }

            text_PageNo.Text = pageNo.ToString();
        }

        /// <summary>
        /// 次ページに移動するボタンの処理
        /// ページ番号を更新する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_nextPage_Click(object sender, EventArgs e)
        {
            // ページNO　現在ページ数 + 1
            uint pageNo = C_GlobalData.string_2_uint32(text_PageNo.Text) + 1;
            if (pageNo < 1)
            {
                pageNo = 1;             // ページNO の最小は 1
            }

            try
            {
                uint maxPage = uint.Parse(label_MaxPage.Text);
                if (pageNo > maxPage)
                {
                    pageNo = maxPage;       // ページNO の最大は、ファイルの件数
                }
            }
            catch (Exception)
            {
            }

            text_PageNo.Text = pageNo.ToString();
        }


        /**
         * 印刷キュー
         * 印刷対象の画像ファイル（フルパス）一覧のキュー
         */
        List<Image> printList = new List<Image>();          // 印刷対象の画像のリスト
        uint printNowPage;                                  // 印刷対象の現在ページ

        /** 印刷イベント処理 */
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            try
            {
                // 画像を取得
                Image img = printList[(int)printNowPage];

                // 単位を0.01インチ単位に合わせる
                float imageW = img.Width / 2;         // 画像幅 （ピクセル数 / 200dpi）インチ x100
                float imageH = img.Height / 2;        // 画像高 （ピクセル数 / 200dpi）インチ x100

                // 縦・横の調整 
                // 画像が横向き（幅>長）&& 画像の横がページ内（余白より内側）に収まらないとき、90度回転させる！
                if (imageW > imageH && imageW > e.MarginBounds.Width)
                {
                    img.RotateFlip(RotateFlipType.Rotate90FlipNone);    // 反転させずに時計回りに回転させる
                    var tmp = imageW;
                    imageW = imageH;
                    imageH = tmp;
                }

                // まず、サイズ合わせを行う！ （画像サイズか、ページサイズのうちの最小値）
                float w = Math.Min(imageW, e.MarginBounds.Width);         // 印刷幅 = 画像幅 or ページ幅 の最小
                float h = Math.Min(imageH, e.MarginBounds.Height);        // 印刷高 = 画像高 or ページ高 の最小

                // 縦横比の調整 (印刷範囲の縦横比を、元の画像と同じにする)
                float rate = Math.Min(w / imageW, h / imageH);          // 縮尺 = 印刷幅/画像幅 or 印刷高/画像高 の最小
                w = imageW * rate;
                h = imageH * rate;


                // 印刷位置のセンタリング (余白を調整する)
                // 余白 = 中心位置の差分 = (ページサイズ - 印刷サイズ ) / 2.0
                float x = (e.PageBounds.Width - w) / 2.0f;
                float y = (e.PageBounds.Height - h) / 2.0f;

                // プレビューではなく実際の印刷の時はプリンタの物理的な余白分ずらす
                if (((PrintDocument)sender).PrintController.IsPreview == false)
                {
                    x -= e.PageSettings.HardMarginX;
                    y -= e.PageSettings.HardMarginY;
                }

                // 印刷対象の画像のリストの現在ページを出力
                e.Graphics.DrawImage(img, x, y, w, h);
            }
            catch (Exception)
            {
            }

            printNowPage++;                            // 次のページにすすむ

            try
            {
                if (printNowPage < printList.Count)     // 現在ページ < ページ数 （次のページ有り）
                {
                    e.HasMorePages = true;              // 次ページ＝true
                }
                else
                {
                    e.HasMorePages = false;             // 次ページ＝false
                    printNowPage = 0;                   // 現在のページ＝0 に戻る
                }
            }
            catch (Exception)
            {
                e.HasMorePages = false;                 // 次ページ＝false
                printNowPage = 0;                       // 現在のページ＝0 に戻る
            }
        }



        /// <summary>
        /// 印刷ボタン（1件分）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void print1_Click(object sender, EventArgs e)
        {
            // 印刷を実行する（用紙・プリンタも設定する、プレビューなし）
            printDo(dataGrid.SelectedRows, false);
        }


        /// <summary>
        /// 印刷プレビューボタン（1件分）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void printPreview1_Click(object sender, EventArgs e)
        {
            // 印刷を実行する（用紙・プリンタも設定する、プレビューあり）
            printDo(dataGrid.SelectedRows, true);
        }

        /// <summary>
        /// 印刷プレビューボタン（選択分）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void printPreview2_Click(object sender, EventArgs e)
        {
            try
            {
                var list = new List<DataGridViewRow>();         // 印刷対象の行

                foreach (DataGridViewRow row in dataGrid.Rows)
                {
                    object val = row.Cells["chk"].Value;
                    if (val == null) { }
                    else if (val.GetType() != typeof(bool)) { }
                    else if ((bool)val == true)
                    {
                        list.Add(row);
                    }
                }

                // 印刷を実行する（用紙・プリンタも設定する、プレビューあり）
                printDo(list, true);

            }
            catch (Exception)
            {
            }
        }


        /// <summary> 用紙・プリンタを設定し、DataGridViewで選択した行の画像を印刷 </summary>
        /// <param name="rowList">DataGridViewの行一覧（印刷する画像の フォルダ名・ファイル名の一覧）</param>
        /// <param name="preview">true:プレビューあり。 false:プレビューなし</param>
        private void printDo(IEnumerable rowList, bool preview)
        {
            try
            {
                // 印刷リストの初期化
                printList.Clear();
                printNowPage = 0;

                foreach (DataGridViewRow row in rowList)
                {
                    foreach (var path in C_GlobalData.GetZokushiList(row.Cells["fname"].Value))
                    {
                        Image img = Image.FromFile(path);               // 画像の読み込み
                        printList.Add(img);                             // リストに追加                         
                    }
                }


                if (printList.Count <= 0)
                {
                    MessageBox.Show("印刷対象なし", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    // printDocument.OriginAtMargins = true;

                    //　プリンタ名と余白設定を読み込む
                    string printerName; int printMargin;
                    C_GlobalData.getPrinterSetting(out printerName, out printMargin);


                    // 印刷ドキュメントに、プリンタを設定する。
                    if (printerName.Length > 0) printDocument.PrinterSettings.PrinterName = printerName;

                    // 用紙の向きを設定(横：true、縦：false)
                    printDocument.DefaultPageSettings.Landscape = false;

                    // 余白を0.01インチ単位に変換  (＝ 余白ミリ/ 25.4ミリ * 100) し、設定する
                    printMargin = (checkBox_Margin.Checked) ? (int)((double)printMargin * 100.0 / 25.4) : (0);
                    printDocument.DefaultPageSettings.Margins.Top = printMargin;
                    printDocument.DefaultPageSettings.Margins.Bottom = printMargin;
                    printDocument.DefaultPageSettings.Margins.Left = printMargin;
                    printDocument.DefaultPageSettings.Margins.Right = printMargin;


                    // 用紙設定（A4）
                    foreach (PaperSize ps in printDocument.PrinterSettings.PaperSizes)
                    {
                        if (ps.Kind == PaperKind.A4)
                        {
                            printDocument.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                            break;
                        }
                    }
                }


                if (preview)
                {
                    // 印刷プレビューの実行
                    printPreviewDialog.WindowState = FormWindowState.Maximized;
                    printPreviewDialog.ShowDialog();
                }
                else
                {
                    // 印刷の実行
                    printDocument.Print();
                }




                // 印刷対象のクリア
                foreach (Image img in printList)
                {
                    img.Dispose();
                }
                printList.Clear();

            }
            catch (Exception)
            {
            }

        }

        /// <summary> 全選択 （ON⇔OFF切り替え）</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < dataGrid.Rows.Count; i++)
                {
                    dataGrid["chk", i].Value = checkBoxAll.Checked;
                }
                dataGrid.FirstDisplayedScrollingColumnIndex = 0;
            }
            catch (Exception)
            {
            }

        }



        /// <summary>
        /// 読み込み ボタン 処理
        /// 読み込み ダイアログを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void settingButton_Click(object sender, EventArgs e)
        {
            new ReadDialog().ShowDialog();
        }


        /// <summary>
        /// 印刷設定ボタン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void printSettingButton_Click(object sender, EventArgs e)
        {
            // プリンタ選択ダイアログを開く
            new SelectPrinterDialog().ShowDialog(this);
        }


        /// <summary>
        /// 表示列設定ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            // 表示列設定の実行
            columnSelect();
        }

        /// <summary>
        /// 表示列設定の実行
        /// </summary>
        private void columnSelect()
        {
            using (var dlg = new ColumnSelectDialog())
            {
                foreach (DataGridViewColumn col in dataGrid.Columns)
                {
                    string head = col.HeaderText;
                    if (head != "" && head != "***")
                    {
                        dlg.checkedListBox1.Items.Add(head, col.Visible);
                    }
                }

                dlg.ShowDialog(dataGrid);

                // 表示設定ダイアログのCheckを、データグリッドビューの表示に反映させる。
                if (dlg.DialogResult == DialogResult.OK)
                {
                    foreach (DataGridViewColumn col in dataGrid.Columns)
                    {
                        string head = col.HeaderText;

                        if (dlg.checkedListBox1.Items.Contains(head))
                        {
                            bool check = dlg.checkedListBox1.CheckedItems.Contains(head);
                            col.Visible = check;
                        }
                    }
                }
            }
        }

        /// <summary> 画像のサイズ（縮小表示）のモード一覧 </summary>
        enum PictureSizeMode
        {
            /// <summary> パネルに合わせる（スクロールしない） </summary>
            PanelSize,
            /// <summary> X・Yいずれかのみパネルに合わせる（一方はスクロールする） </summary>
            XorYSize,
            /// <summary> イメージの大きさに合わせる（X・Y両方向でスクロールする） </summary>
            ImageSize,
        } ;

        PictureSizeMode m_ePictureBoxSizeMode = PictureSizeMode.PanelSize;

        /// <summary>
        /// ピクチャーボックスクリック時、スクロールのモードを変える
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox_Click(object sender, EventArgs e)
        {
            // 先に状態遷移させる。
            switch (m_ePictureBoxSizeMode)
            {
                case PictureSizeMode.PanelSize:
                    // パネルに合わせる（スクロールしない）→ X・Yいずれかのみパネルに合わせる（一方はスクロールする）
                    m_ePictureBoxSizeMode = PictureSizeMode.XorYSize;
                    break;
                case PictureSizeMode.XorYSize:
                    // X・Yいずれかのみパネルに合わせる（一方はスクロールする）→ イメージの大きさに合わせる（X・Y両方向でスクロールする）
                    m_ePictureBoxSizeMode = PictureSizeMode.ImageSize;
                    break;
                case PictureSizeMode.ImageSize:
                default:
                    // イメージの大きさに合わせる（X・Y両方向でスクロールする）→パネルに合わせる（スクロールしない） 
                    m_ePictureBoxSizeMode = PictureSizeMode.PanelSize;
                    break;
            }

            switch (m_ePictureBoxSizeMode)
            {
                case PictureSizeMode.PanelSize:
                    // パネルに合わせる（スクロールしない）
                    pictureBox.Dock = DockStyle.Fill;                   // ピクチャーボックスのサイズを、枠に合わせる
                    pictureBox.SizeMode = PictureBoxSizeMode.Zoom;      // 画像の大きさを、自動縮小（ピクチャーボックスに合わせる）
                    break;
                case PictureSizeMode.XorYSize:
                    // X・Yいずれかのみパネルに合わせる（一方はスクロールする）
                    pictureBox.SizeMode = PictureBoxSizeMode.Zoom;      // 画像の大きさを、自動縮小（ピクチャーボックスに合わせる）
                    break;
                case PictureSizeMode.ImageSize:
                    // イメージの大きさに合わせる（X・Y両方向でスクロールする）
                    pictureBox.Dock = DockStyle.Fill;
                    pictureBox.Dock = DockStyle.None;                   // ピクチャーボックスのサイズを、枠に合わせない
                    pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;  // ピクチャーボックスの大きさを、画像に合わせる
                    break;
                default:
                    break;
            }
            updatePictureBoxSize();
        }

        /// <summary>
        /// ピクチャーボックスのイメージ読み込み完了時、全体表示（スクロールなし）に切り替える
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            updatePictureBoxSize();
        }

        /// <summary>
        /// ピクチャーボックス用のパネルサイズ変更時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panel_PictureBox_SizeChanged(object sender, EventArgs e)
        {
            updatePictureBoxSize();
        }

        /// <summary>
        /// ピクチャーボックスのパネルサイズ変更時・イメージサイズ変更時に、高さ・幅を調整する。
        /// </summary>
        private void updatePictureBoxSize()
        {
            if (pictureBox.Image == null) return;

            // X・Yいずれかのみパネルに合わせる（一方はスクロールする）モードの時のみ、調整する
            if (m_ePictureBoxSizeMode == PictureSizeMode.XorYSize)
            {
                try
                {
                    // 比率 (枠のサイズ / 画像のサイズ) を計算する
                    double rate_X = (double)panel_PictureBox.Width / (double)pictureBox.Image.Size.Width;
                    double rate_Y = (double)panel_PictureBox.Height / (double)pictureBox.Image.Size.Height;

                    if (rate_X > rate_Y)
                    {
                        // X方向の比率 > Y方向の比率の時               
                        pictureBox.Dock = DockStyle.Top;                                // ピクチャーの幅を枠に合わせる
                        pictureBox.Height = (int)(pictureBox.Image.Height * rate_X);    // ピクチャーの高さを、X方向の比率に合わせる
                    }
                    else
                    {
                        //                        pictureBox.Height = panel_PictureBox.Height;
                        pictureBox.Dock = DockStyle.Left;                               // ピクチャーの幅を枠に合わせる
                        pictureBox.Width = (int)(pictureBox.Image.Width * rate_Y);      // ピクチャーの幅を、Y方向の比率に合わせる
                    }
                }
                catch (Exception)
                {
                }
            }

        }

        private void dataGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
        }
        

        private void insuredno_Validated(object sender, EventArgs e)
        {
            UpdateInsuredNameList();
        }

        /// <summary> 氏名一覧の初期化 </summary>
        void UpdateInsuredNameList()
        {
            int no;
            if(int.TryParse(insuredno.Text, out no)==false) no = -1;
            string query = "SELECT DISTINCT insuredname FROM t_data WHERE medimonth > 0 AND medimonth < 9999";
            if(no > 0) query += " AND insuredno = " + no;
            query += " ORDER BY insuredname";


            var list = new List<string>();
            using (var cmd = C_GlobalData.sqlCommand(query))
            using (var r = cmd.ExecuteReader())
            {
                //20190614112132 furukawa st ////////////////////////
                //遅いので設定方法変更

                while (r.Read()) insuredname.Items.Add(r[0].ToString());
                        //while (r.Read()) list.Add(r[0].ToString());
                //20190614112132 furukawa ed ////////////////////////
            }

            //20190614112700 furukawa st ////////////////////////
            //不要

                    //SetComplete(insuredname, list);
            //20190614112700 furukawa ed ////////////////////////


        }

        /// <summary> 支払先一覧の初期化 </summary>
        void UpdateBillList()
        {
            var list = new List<string>();
            using (var cmd = C_GlobalData.sqlCommand("SELECT DISTINCT billname FROM t_data WHERE medimonth > 0 AND medimonth < 9999 order by billname "))
            using (var r = cmd.ExecuteReader())
            {
                //20190614112222 furukawa st ////////////////////////
                //遅いので設定方法変更

                while (r.Read()) billname.Items.Add(r[0].ToString());
                        //while (r.Read()) list.Add(r[0].ToString());
                //20190614112222 furukawa ed ////////////////////////
            }
            //20190614112607 furukawa st ////////////////////////
            //不要

                    //SetComplete(billname, list);
            //20190614112607 furukawa ed ////////////////////////
        }

        private void billname_Validated(object sender, EventArgs e)
        {
            UpdateHosList();
        }

        /// <summary> 施術院の初期化 </summary>
        void UpdateHosList()
        {
            var list = new List<string>();
            using (var cmd = C_GlobalData.sqlCommand("SELECT DISTINCT hosname FROM t_data WHERE billname Like '" + billname.Text.Trim() + "%' AND medimonth > 0 AND medimonth < 9999 order by hosname"))
            using (var r = cmd.ExecuteReader())
            {
                //20190614112301 furukawa st ////////////////////////
                //遅いので設定方法変更

                while (r.Read()) hosname.Items.Add(r[0].ToString());
                        //while (r.Read()) list.Add(r[0].ToString());
                //20190614112301 furukawa ed ////////////////////////
            }
            
            
            //20190614112417 furukawa st ////////////////////////
            //不要

                    //    SetComplete(hosname, list);
            //20190614112417 furukawa ed ////////////////////////

        }

        /// <summary> SQLのReader → コンボボックスの入力補完 </summary>
        /// <param name="c">コンボボックス</param>
        /// <param name="list">テキスト一覧</param>
        void SetComplete(ComboBox c, List<string> list)
        {
            string prev = c.Text;

            if (c.DataSource as BindingSource == null)
            {
                c.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                c.AutoCompleteSource = AutoCompleteSource.ListItems;
                c.DataSource = new BindingSource();
            }
            var s = c.DataSource as BindingSource;

            s.Clear();
            foreach (var item in list) s.Add(item);

            c.Text = prev;
        }

        private void buttonCLS_Click(object sender, EventArgs e)
        {
            insuredno.Clear();
            insuredname.Text = "";
            billname.Text = "";
            hosname.Text = "";
            seek_MediDateMin.Clear();
            seek_MediDateMax.Clear();
            seek_BillDateMin.Clear();
            seek_BillDateMax.Clear();

            UpdateInsuredNameList();
            UpdateHosList();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) checkBox2.Checked = false;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked) checkBox1.Checked = false;
        }

    }
}
