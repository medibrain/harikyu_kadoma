﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ReceSeek_Harikyu
{
    public partial class SelectPrinterDialog : Form
    {
        public SelectPrinterDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// OKボタンを押した時の処理  プリンタ設定を変更し、ダイアログを閉じる
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {            
            C_GlobalData.writePrinterSetting(comboBox_SelectPrinter.Text, (int)numericUpDown_Margin.Value);
            this.Close();
        }

        /// <summary>
        /// キャンセルボタンを押した時の処理　ダイアログを閉じる
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// フォームの初期化　プリンタ一覧を表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectPrinterDialog_Load(object sender, EventArgs e)
        {         
            //コンピュータにインストールされているすべてのプリンタの名前を出力する
            foreach (string s in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                comboBox_SelectPrinter.Items.Add(s);
            }

            // XMLからプリンタ名・余白を取得し、ダイアログに反映させる
            string printerName; int printMargin;
            C_GlobalData.getPrinterSetting(out printerName, out printMargin);
            comboBox_SelectPrinter.Text = printerName;      // プリンタ名選択のコンボボックス
            numericUpDown_Margin.Value = printMargin;       // 余白設定
                        
            // コンボボックスに設定されなかった時 （XMLからプリンタ名が取得できない時）は、通常使うプリンタにする
            if (comboBox_SelectPrinter.Text.Length <= 0)
            {
                using (System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument())
                {
                    comboBox_SelectPrinter.Text = pd.PrinterSettings.PrinterName;
                }
            }
        }
    }
}
