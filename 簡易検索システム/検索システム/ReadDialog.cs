﻿using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using System.Net;
using System.Data.Common;

namespace ReceSeek_Harikyu
{
    /// <summary>
    /// DVD読み込みダイアログ
    /// </summary>
    public partial class ReadDialog : Form
    {
        public ReadDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ダイアログ初期化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReadDialog_Load(object sender, EventArgs e)
        {
            try
            {
                // ドライブ選択のコンボボックスの設定
                combo_DrivaName.Items.Clear();                              // クリア
                foreach (string drive in Directory.GetLogicalDrives())      // ローカルドライブをコンボボックスに追加 (例： D:\)
                {
                    combo_DrivaName.Items.Add(drive);
                }
                combo_DrivaName.Text = C_GlobalData.driveName;              // ドライブ名は、XMLから取得 （C_GlobalData のプロパティ）

                label_Message_1.Text = "";
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// ダイアログ終了時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReadDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                // コンボボックスで選択したドライブ名を、XMLに出力 （C_GlobalData のプロパティ）
                C_GlobalData.driveName = combo_DrivaName.Text;


                // バックグラウンド処理が実行中のとき、中断確認をする
                if (backgroundWorker1.IsBusy)
                {
                    // 中断確認のダイアログを表示
                    if (MessageBox.Show("読み込み途中です。中断しますか？",
                                         "読み込み中断",
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        // YESボタンで、バックグラウンド処理ををキャンセルさせる
                        backgroundWorker1.CancelAsync();
                    }

                    // バックグラウンド処理が実行中 （終了しなかった）の場合、ダイアログは閉じない
                    //（スレッドキャンセルのイベントで閉じる）
                    if (backgroundWorker1.IsBusy) e.Cancel = true;
                }
            }
            catch (Exception)
            {
            }
        }


        /// <summary>
        /// OKボタン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            // 2重実行防止 (スレッドが起動中で無い場合のみ動作させる！)
            if (backgroundWorker1.IsBusy) return;

            // ドライブ名をXMLに出力 例  D:\
            string drive = combo_DrivaName.Text;
            C_GlobalData.driveName = drive;
            
            // 読み込みのバックグラウンド処理の開始 （引数として、ドライブ名を渡す。）
            buttonOK.Enabled = false;                       // OKボタンの無効化
            label_Message_1.Text = "読み込み中...";
            backgroundWorker1.RunWorkerAsync(combo_DrivaName.Text);
        }


        /// <summary> ファイルを探す </summary>
        /// <param name="root">ルート      例：D:\ </param>
        /// <param name="fname">ファイル名</param>
        /// <returns>フルパス（ルート直下 D:\ファイル名 or 1階層下 D:\サブ\ファイル名）</returns>
        string SeekFile(string root, string fname)
        {
            try
            {
                string path = (root + @"\" + fname).Replace(@"\\", @"\");
                if (File.Exists(path)) return path;

                foreach (var sub in Directory.GetDirectories(root))
                {
                    path = (sub + @"\" + fname).Replace(@"\\", @"\");
                    if (File.Exists(path)) return path;
                }

            }
            catch (Exception)
            {
            }

            return "";
        }


        /// <summary> ディレクトリを探す </summary>
        /// <param name="root">ルート          例：D:\ </param>
        /// <param name="name">ディレクトリ名  例：img </param>
        /// <returns>フルパス（ルート直下 D:\img or 1階層下 D:\サブ\img）</returns>
        string SeekDir(string root, string name)
        {
            try
            {
                string path = (root + @"\" + name).Replace(@"\\", @"\");
                if (Directory.Exists(path)) return path;

                foreach (var sub in Directory.GetDirectories(root))
                {
                    path = (sub + @"\" + name).Replace(@"\\", @"\");
                    if (Directory.Exists(path)) return path;
                }
            }
            catch (Exception)
            {
            }

            return "";
        }

        string[] FileList(string root, string pattarn)
        {
            var list = new List<string>();

            foreach (var dir1 in Directory.GetDirectories(root))
            {
                foreach (var dir2 in Directory.GetDirectories(dir1))
                {
                    list.AddRange(Directory.GetFiles(dir2, pattarn));
                }
            }
            return list.ToArray();
        }
         


        /// <summary>
        /// 読み込みのバックグラウンド処理処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"> backgroundArg でキャストする</param>
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            string drive = e.Argument.ToString();               // 読込元ドライブ （例: D:\ ）

            e.Cancel = false;                                   // キャンセル = false

            string fnCsv = SeekFile(drive, "receiptdata.csv");
            if (fnCsv != "") ReadCsv(fnCsv);
            
            // 画像ファイルの取込
            string imgDir = SeekDir(drive, "img");                          // ドライブ名\img
            if (imgDir != "")
            {
                string[] fileList = FileList(imgDir, "*.gif");
                int cnt = fileList.Length;                                  // ファイル数

                for (int i = 0; i < cnt; i++)
                {
                    // キャンセルされてないか定期的にチェック
                    if (((BackgroundWorker)sender).CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }

                    try
                    {
                        string fromPath = fileList[i];                              // 例： D:\img\グループ名\ファイル名.gif
                        string destPath = C_PostgreSettings.PictureRootPath +       // 保存先のROOT
                                          @"\img\" +                                //             \img\
                                          fromPath.Replace(imgDir, "");             // 読み込み元と同じ （但し、D:\img\を除外）
                        
                        destPath = destPath.Replace(@"\\", @"\");
                        string destDir = Path.GetDirectoryName(destPath);

                        if (Directory.Exists(destDir) == false) Directory.CreateDirectory(destDir);
                        File.Copy(fromPath, destPath, true);

                        // プログラスバーへの反映
                        ((BackgroundWorker)sender).ReportProgress((int)(100 * (i + 1) / cnt));
                    }
                    catch (Exception)
                    {
                    }
                }
            } //end if (imgDir != "")


            // プログラスバーへの反映
            ((BackgroundWorker)sender).ReportProgress(100);
        }


        /// <summary> CSVファイルを読込、DBに登録する </summary>
        /// <param name="path">CSVファイル名</param>
        void ReadCsv(string path)
        {
            // CSVファイル用のバッファへの読み込み
            // insert : 出力日・グループ名（フォルダ名）・ファイル名・請求年月・支払先・施術院名・診療年月・被保険者番号・患者名を登録する。
            using (var ins = C_GlobalData.sqlCommand("INSERT INTO t_data ( timestamp,  fname,  billmonth,  billname,  hosname,  medimonth,  insuredno,  insuredname,  typecode)" +
                                                     "            VALUES (:timestamp, :fname, :billmonth, :billname, :hosname, :medimonth, :insuredno, :insuredname, :typecode)"))
            // update : 出力日・請求年月・支払先・施術院名・診療年月・被保険者番号・患者名を変更する。
            //          ファイル名で突合させる。 変更後のtimestampが新しい場合のみ更新する。
            using (var upd = C_GlobalData.sqlCommand("UPDATE t_data SET timestamp=:timestamp, billmonth=:billmonth, billname=:billname, " +
                                                     "hosname=:hosname,  medimonth=:medimonth, insuredno=:insuredno, insuredname=:insuredname, typecode=:typecode " +
                                                     "WHERE fname=:fname AND timestamp<=:timestamp"))
            // chk     : SELECTコマンドにて、ファイル名が重複しているかをチェックする。
            using (var chk = C_GlobalData.sqlCommand("SELECT count(*) FROM t_data WHERE fname = :fname"))
            {
                DbCommand w;

                foreach (var c in new DbCommand[] { ins, upd, chk })
                {
                    C_GlobalData.AddDBParam(c, "timestamp", C_GlobalData.e_dbType.Integer);
                    C_GlobalData.AddDBParam(c, "fname", C_GlobalData.e_dbType.Integer);
                    C_GlobalData.AddDBParam(c, "billmonth", C_GlobalData.e_dbType.Integer);
                    C_GlobalData.AddDBParam(c, "billname", C_GlobalData.e_dbType.Text);
                    C_GlobalData.AddDBParam(c, "hosname", C_GlobalData.e_dbType.Text);
                    C_GlobalData.AddDBParam(c, "medimonth", C_GlobalData.e_dbType.Integer);
                    C_GlobalData.AddDBParam(c, "insuredno", C_GlobalData.e_dbType.Integer);
                    C_GlobalData.AddDBParam(c, "insuredname", C_GlobalData.e_dbType.Text);
                    C_GlobalData.AddDBParam(c, "typecode", C_GlobalData.e_dbType.Text);
                }

#if SQLIGHT
                using (var tran = C_GlobalData.sqlBeginTransaction())
#endif
                {

                    // CSVを１ファイル読み込み
                    foreach (var line in File.ReadAllLines(path, Encoding.Default))
                    {
                        // CSVを１行読み込み
                        string[] cells = line.Split(',');
                        if (cells.Length < 8) continue;

                        int timestamp, fname, medimonth;
                        if (int.TryParse(cells[0].Trim(), out timestamp) == false) continue;    // 0：タイムスタンプ
                        if (int.TryParse(cells[1].Trim(), out fname) == false) continue;        // 1：ファイル名
                        if (int.TryParse(cells[5].Trim(), out medimonth) == false) continue;    // 5：診療年月

                        // 重複チェック → INSERT or UPDATE を決定する。
                        chk.Parameters["fname"].Value = fname;
                        using (var r = chk.ExecuteReader())
                        {
                            r.Read();
                            if (Convert.ToInt32(r[0]) <= 0) w = ins; // 重複データなし → INSERTを実行
                            else w = upd;                            // 重複データあり → UPDATEを実行
                        }


                        w.Parameters["timestamp"].Value = timestamp;
                        w.Parameters["fname"].Value = fname;
                        w.Parameters["medimonth"].Value = medimonth;


                        if (medimonth > 0 && medimonth < 9999)
                        {
                            int billmonth, insuredno;
                            string billname, hosname, insuredname;
                            if (int.TryParse(cells[2].Trim(), out billmonth) == false) continue;    // 2：請求月
                            if ((billname = (cells[3].Trim())).Length == 0) continue;               // 3：支払先
                            if ((hosname = (cells[4].Trim())).Length == 0) continue;                // 4：施術院
                            if (int.TryParse(cells[6].Trim(), out insuredno) == false) continue;    // 6：被保険者番号
                            if ((insuredname = (cells[7].Trim())).Length == 0) continue;            // 7：氏名
                            w.Parameters["billmonth"].Value = billmonth;
                            w.Parameters["billname"].Value = billname;
                            w.Parameters["hosname"].Value = hosname;
                            w.Parameters["insuredno"].Value = insuredno;
                            w.Parameters["insuredname"].Value = insuredname;


                            
                            // typecode 1鍼灸, 2あんま の追加 （デフォルトは１）
                            if (cells.Length < 9) w.Parameters["typecode"].Value = 1;               // デフォルト１鍼灸
                            else if (cells[8].Trim() == "2") w.Parameters["typecode"].Value = 2;    // ２あんま
                            else w.Parameters["typecode"].Value = 1;                                // １鍼灸
                        }
                        else
                        {
                            w.Parameters["billmonth"].Value = DBNull.Value;
                            w.Parameters["billname"].Value = DBNull.Value;
                            w.Parameters["hosname"].Value = DBNull.Value;
                            w.Parameters["insuredno"].Value = DBNull.Value;
                            w.Parameters["insuredname"].Value = DBNull.Value;
                            w.Parameters["typecode"].Value = DBNull.Value;                          // typecode 1鍼灸, 2あんま の追加 （続紙・エラーの場合はNULL）
                        }

                        w.ExecuteNonQuery();
                    } // End foreach (var line in File.ReadAllLines(fnCsv, Encoding.Default))
#if SQLIGHT
                    tran.Commit();
#endif
                } // End using (var tran = C_GlobalData.sqlBeginTransaction())
            }
        }

        /// <summary>
        /// OKボタンバックグラウンドの終了処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Close();
            if (e.Cancelled == false)
            {
                MessageBox.Show("読み込み完了");
            }
        }
        
        /// <summary>
        /// キャンセルボタン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// ProgressBarの更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }
    }
}
