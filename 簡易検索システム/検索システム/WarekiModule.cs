﻿using System;

/// <summary>
/// 和暦変換クラス
/// </summary>
class WarekiModule
{

    //20190614111151 furukawa st ////////////////////////
    //令和追加
    
    /// <summary> 元号コードの最大 </summary>
    static private int maxEra = 5;

    /// <summary> 各元号の元年の西暦年一覧 </summary>
    static private uint[] warekiTable = { 1867, 1911, 1925, 1988, 2018};

    /// <summary> 元号名一覧一覧 </summary>
    static private string[] erNames = {"M.", "T.", "S." ,"H." ,"R."};

            /*
            /// <summary> 元号コードの最大 </summary>
            static private int maxEra = 4;

            /// <summary> 各元号の元年の西暦年一覧 </summary>
            static private uint[] warekiTable = { 1867, 1911, 1925, 1988 };

            /// <summary> 元号名一覧一覧 </summary>
            static private string[] erNames = { "M.", "T.", "S.", "H." };
            */

    //20190614111151 furukawa ed ////////////////////////

    /// <summary>
    /// 和暦→西暦変換
    /// </summary>
    /// <param name="era">元号コード</param>
    /// <param name="year">元号年</param>
    /// <returns>西暦年 (エラー時は-1)</returns>
    public static int wareki_2_seireki(int era, int year)
    {
        try
        {
            return (int)warekiTable[era - 1] + year;
        }
        catch (Exception)
        {
            return -1;
        }
    }


    /// <summary>
    /// 日付 （eYY/MM/DD or YYYY/MM/DD）→  SQLコマンド 'YYYY/MM/DD' （最小絞り込み用）の変換
    /// </summary>
    /// <param name="e">元号 （未入力時は-1）</param>
    /// <param name="y">年   （未入力時は-1）</param>
    /// <param name="m">月   （未入力時は-1）</param>
    /// <param name="d">日   （未入力時は-1）</param>
    /// <returns>SQLコマンド </returns>
    static string date_2_sqlMin(int e, int y, int m, int d)
    {
        try
        {
            if (e >= 1 && e <= maxEra)      // 元号あり
            {
                if (y == 0 || y < 0)        // ゼロ年 or 年なし
                {
                    return "'" + wareki_2_seireki(e, 1).ToString("0000") + "-01-01'";       // e.00/MM/DD 以上 or e.以上 →  e.01-01-01 以上
                }

                y = wareki_2_seireki(e, y);                                                 // 和暦→西暦変換
            }

            // 西暦年チェック
            if (y < 1800 || y > 2100) return "";                                            // SQLの範囲外
            
            // 月チェック
            if (m > 12)
            {
                y++;
                return "'" + y.ToString("0000") + "-01-01'";                                // YYYY年13月以上 → 翌年1月1日以上
            }
            else if (m < 1)
            {
                return "'" + y.ToString("0000") + "-01-01'";                                // YYYY年0月以上 or YYYY年以上 → YYYY年1月1日以上
            }

            // 日チェック
            if (d <= 0)  d = 1;                                                             // MM月以上(日が未入力) or MM月0日以上  → MM月1日以上とみなす
            
            if (d > DateTime.DaysInMonth(y, m))                                             // MM月32日以上 (末日超え) → 翌月1日以上に変換
            {
                m++;
                d = 1;                                                                      // MM月以下 or MM月32日以下 → 翌月1日以下  (1日未満)
            }

            if (m > 12)                                                                     // YYYY年13月DD日以上 → 翌年１月１日以上
            {
                m = 1;
                d = 1;
                y++;
            }

            return "'" + y.ToString("0000") + "-" + m.ToString("00") + "-" + d.ToString("00") + "'";
            
        }
        catch (Exception)
        {
            return "";
        }
    }


    /// <summary>
    /// 日付 （eYY/MM/DD or YYYY/MM/DD）→  SQLコマンド 'YYYY/MM/DD' （最大絞り込み用）の変換
    /// </summary>
    /// <param name="e">元号 （未入力時は-1）</param>
    /// <param name="y">年   （未入力時は-1）</param>
    /// <param name="m">月   （未入力時は-1）</param>
    /// <param name="d">日   （未入力時は-1）</param>
    /// <returns>SQLコマンド </returns>
    static string date_2_sqlMax(int e, int y, int m, int d)
    {
        try
        {
            if (e >= 1 && e <= maxEra)      // 元号あり
            {
                if (y == 0)                 // ゼロ年
                {
                    return "'" + wareki_2_seireki(e, 1).ToString("0000") + "-01-01'";       // e.00/MM/DD 以下 → e.01/MM/DD 未満
                }
                else if (y < 0)              // 年なし
                {
                    if (e == maxEra) return "";                                             // H. 以下 → なんでもあり
                    e++;                                                                    // 翌元号 （M. T. S.  → T. S. H.)
                    return "'" + wareki_2_seireki(e, 2).ToString("0000") + "-01-01'";       // M. T. S. 以下 → T.02/01/01 S.02/01/01 H.02/01/01 未満
                }

                y = wareki_2_seireki(e, y);                                                 // 和暦→西暦変換
            }


            // 西暦年チェック
            if (y < 1800 || y > 2100) return "";                                            // SQLの範囲外


            // 月チェック
            if (m < 0 || m > 12)                                                            // 月が未入力　・ 13以上
            {
                y++;
                return "'" + y.ToString("0000") + "-01-01'";                                // YYYY年以下 or YYYY年13月以下 → 翌年1月1日未満
            }
            else if (m == 0)
            {
                return "'" + y.ToString("0000") + "-01-01'";                                // YYYY年0月以下  → YYYY年1月1日未満
            }

            // 日チェック
            if (d < 0) d = 31;                                                             // MM月以下（日が未入力 ） → MM月末日以下とみなす

            d++;                                                                            // YYYY/MM/DD 以下 → 翌日未満に変換する （０～３１日以下→1～３２日未満）

            if (d > DateTime.DaysInMonth(y, m))                                             // MM月32日未満 (末日超え) → 翌月1日未満に変換
            {
                m++;
                d = 1;                                                                      // MM月以下 or MM月32日以下 → 翌月1日以下  (1日未満)
            }

            if (m > 12)                                                                     // YYYY年13月DD日未満 → 翌年１月１日未満
            {
                m = 1;
                d = 1;
                y++;
            }

            return "'" + y.ToString("0000") + "-" + m.ToString("00") + "-" + d.ToString("00") + "'";
        }
        catch (Exception)
        {
            return "";
        }
    }

    /// <summary>
    /// eYYMMDD → 'YYYY/MM/DD' 変換 （SQL MIN検索値）検索用
    /// </summary>
    /// <param name="text">eYYMMDD</param>
    /// <returns>SQLコマンド</returns>
    static string warekiNum_2_sqlMin(string text)
    {
        int e = -1;
        int y = -1;
        int m = -1;
        int d = -1;

        try
        {
            e = int.Parse(text.Substring(0, 1));
            y = int.Parse(text.Substring(1, 2));
            m = int.Parse(text.Substring(3, 2));
            d = int.Parse(text.Substring(5, 2));
        }
        catch (Exception)
        {
        }

        return date_2_sqlMin(e, y, m, d);
    }

    /// <summary>
    /// eYYMMDD → 'YYYY/MM/DD' 変換 （SQL MAX検索値）検索用
    /// </summary>
    /// <param name="text">eYYMMDD</param>
    /// <returns>SQLコマンド</returns>
    static string warekiNum_2_sqlMax(string text)
    {
        int e = -1;
        int y = -1;
        int m = -1;
        int d = -1;

        try
        {
            e = int.Parse(text.Substring(0, 1));
            y = int.Parse(text.Substring(1, 2));
            m = int.Parse(text.Substring(3, 2));
            d = int.Parse(text.Substring(5, 2));
        }
        catch (Exception)
        {
        }

        return date_2_sqlMax(e, y, m, d);
    }

    
    static public string warekiNum_2_sqlMaxMin(string min, string max, string colName)
    {
        min = min.Trim();
        max = max.Trim();

        if (min.Length == 0 && max.Length == 0) return "";      // どちらも空白 → 計算しない
        if (min.Length == 0) min = max;                         // min が空白   → max 以下 max 以上で計算する
        if (max.Length == 0) max = min;                         // max が空白   → min 以下 min 以上で計算する




        // 西暦年（SQL比較用）に変換する（min, maxに合わせる）
        string sql_min = warekiNum_2_sqlMin(min);
        string sql_max = warekiNum_2_sqlMax(max);

        // 西暦年MIN > 西暦年MAXのとき、MINとMAXを入れ替えて、再度変換する
        if (string.Compare(sql_min, sql_max) > 0 && sql_min.Length > 0 && sql_max.Length > 0)
        {
            sql_min = warekiNum_2_sqlMin(max);
            sql_max = warekiNum_2_sqlMax(min);
        }
        
        if (sql_min.Length == 0 && sql_max.Length == 0)
        {
            return "";
        }
        else if (sql_max.Length == 0)
        {
            return colName + ">=" + sql_min;
        }
        else if (sql_min.Length == 0)
        {
            return colName + "<" + sql_max;
        }
        else
        {
            return colName + ">=" + sql_min + " AND " + colName + "<" + sql_max;
        }
    }
    


    /// <summary>
    /// 日付検索用テキストボックス（最小値入力用）の入力 → SQLコマンド 'YYYY/MM/DD' の変換
    /// </summary>
    /// <param name="text">対象となるテキストボックスの文字 </param>
    /// <returns>'YYYY/MM/DD'：正常値 / 空白：異常値</returns>
    static public string gymd_2_sqlMin(string text)
    {
        // 元号、年、月、日の取得
        int e = -1, y = -1, m = -1, d = -1;

        try
        {
            string[] ymd = text.Split('/');  // YYYY/MM/DD or gYY/MM/DD を / で区切る
            string str_y = ymd.Length > 0 ? ymd[0] : text;



            // 元号を取得 
            for (int i = 0; i < maxEra; i++)
            {
                if (str_y.IndexOf(erNames[i]) >= 0)         // 年に元号が含まれている
                {
                    e = i + 1;                              // 元号が見つかった！
                    str_y = str_y.Replace(erNames[i], "");  // 元号名はいらないので削除
                    break;
                }
            }

            // 年・月・日を取得
            y = int.Parse(str_y);
            m = int.Parse(ymd[1]);
            d = int.Parse(ymd[2]);
        }
        catch (Exception)
        {
        }

        return date_2_sqlMin(e, y, m, d);
    }



    /// <summary>
    /// テキスト表示（YYYY/MM/DD or gYY/MM/DD） → SQLコマンド 'YYYY/MM/DD' の変換
    /// </summary>
    /// <param name="text">テキストボックス表示 </param>
    /// <returns>'YYYY/MM/DD'：正常値 / 空白：異常値</returns>
    static public string gymd_2_sqlMax(string text)
    {
        // 元号、年、月、日の取得
        int e = -1, y = -1, m = -1, d = -1;
        try
        {
            string[] ymd = text.Split('/');  // YYYY/MM/DD or gYY/MM/DD を / で区切る
            string str_y = ymd.Length > 0 ? ymd[0] : text;

            // 元号を取得 
            for (int i = 0; i < maxEra; i++)
            {
                if (str_y.IndexOf(erNames[i]) >= 0)         // 年に元号が含まれている
                {
                    e = i + 1;                              // 元号が見つかった！
                    str_y = str_y.Replace(erNames[i], "");  // 元号名はいらないので削除
                    break;
                }
            }

            // 年・月・日を取得
            y = int.Parse(str_y);
            m = int.Parse(ymd[1]);
            d = int.Parse(ymd[2]);
        }
        catch (Exception)
        {
        }

        return date_2_sqlMax(e, y, m, d);
    }


    static public string gymd_2_sqlMaxMin(string min, string max, string colName)
    {
        min = min.Trim();
        max = max.Trim();

        if (min.Length == 0 && max.Length == 0) return "";      // どちらも空白 → 計算しない
        if (min.Length == 0) min = max;                         // min が空白   → max 以下 max 以上で計算する
        if (max.Length == 0) max = min;                         // max が空白   → min 以下 min 以上で計算する

        // 西暦年（SQL比較用）に変換する（min, maxに合わせる）
        string sql_min = gymd_2_sqlMin(min);
        string sql_max = gymd_2_sqlMax(max);

        // 西暦年MIN > 西暦年MAXのとき、MINとMAXを入れ替えて、再度変換する
        if (string.Compare(sql_min, sql_max) > 0 && sql_min.Length > 0 && sql_max.Length > 0)
        {
            sql_min = gymd_2_sqlMin(max);
            sql_max = gymd_2_sqlMax(min);
        }

        if (sql_min.Length == 0 && sql_max.Length == 0)
        {
            return "";
        }
        else if (sql_max.Length == 0)
        {
            return colName + ">=" + sql_min;
        }
        else if (sql_min.Length == 0)
        {
            return colName + "<" + sql_max;
        }
        else
        {
            return colName + ">=" + sql_min + " AND " + colName + "<" + sql_max;
        }
    }
}

