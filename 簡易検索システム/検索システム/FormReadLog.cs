﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ReceSeek_Harikyu
{
    public partial class FormReadLog : Form
    {
        public FormReadLog()
        {
            InitializeComponent();
        }

        private void FormReadLog_Load(object sender, EventArgs e)
        {
            textBox1.Text = System.Reflection.Assembly.GetEntryAssembly().Location + "\r\n\r\n" +
                            "＜バージョン情報＞\r\n" +
                            "製品名      ：" + Application.ProductName + "\r\n" +
                            "バージョン名：" + Application.ProductVersion + "\r\n" +

// 役所名 （K門真市、W和歌山広域）
#if KADOMA_CITY
                            "・門真市役所用\r\n" + 
#elif WAKA_KOIKI
                            "・和歌山広域用\r\n" +
#else
#error　コンパイルエラー！ コンパイルスイッチにて、役所名を指定してください
#endif

// エンジン （P PostgreSql、L SQLight）
#if POSTGRESQL
                            "・PostgreSQL\r\n" +
#elif SQLIGHT
                            "・SQLight\r\n" +
#else
#error　コンパイルエラー！ コンパイルスイッチにて、SQLのエンジンを指定してください
#endif

#if DEBUG
                            "・Debug\r\n" +
#endif


                            "\r\n＜画像フォルダ＞\r\n" + C_PostgreSettings.PictureRootPath + "\r\n\r\n"  +
                            "＜DB接続＞\r\n" + C_PostgreSettings.ConnectText.Replace(";", ";\r\n");
        }
    }
}
