﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using System.Data.Common;
#if POSTGRESQL
using Npgsql;
#elif SQLIGHT
using System.Data.SQLite;
#endif
using System.Text;


namespace ReceSeek_Harikyu
{
    
    /// <summary> PostgreSQLの設定情報 </summary>
    public class C_PostgreSettings
    {
        /// <summary> PostgreSQL.ini の読込結果 </summary>
        /// [0] 画像フォルダパス
        /// [1～] 接続文字列
        static string[] m_data = null;

        /// <summary> PostgreSQL.ini を読み込む </summary>
        static void Read()
        {
#if POSTGRESQL
            string path = C_GlobalData.getAppDir() + @"\PostgreSQL.ini";
#elif SQLIGHT
            string path = C_GlobalData.getAppDir() + @"\SQLight.ini";
#endif
            m_data = File.ReadAllLines(path, Encoding.Default);
        }



#if SQLIGHT
        /// <summary> SQLight DBファイル（フルパス名） </summary>
        static public string SQLightDBPath
        {
            get { return PictureRootPath + @"\receiptdata.db"; }
        }
#endif

        /// <summary> SQL 接続用文字列 </summary>
        static public string ConnectText
        {
            get
            {
#if POSTGRESQL
                if (m_data == null) Read();
                
                // IDアドレスを取得する
                string tmp = "";
                for (int i = 1; i < m_data.Length; i++) tmp += m_data[i];
                return tmp;
#elif SQLIGHT
                if (m_data == null) Read();
                return "Data Source=" + SQLightDBPath;
#endif
            }
        }
        
        /// <summary> 画像フォルダパス </summary>
        static public string PictureRootPath
        {
            get
            {
                if (m_data == null) Read();
                return m_data[0].Replace("*", C_GlobalData.getAppDir());
            }
        }
    }

    /// <summary>
    /// 設定データを格納するクラス
    /// (XMLファイルに読み書きする）     
    /// </summary>    
    public class C_SettingData
    {
        /// <summary> DVDドライブ名のパス </summary>
        public string driveName = "";

        /// <summary> 印刷用プリンタ名 </summary>
        public string pinterName = "";

        /// <summary> 印刷時の余白（ミリ） </summary>
        public int printMargin = 5;
    }


    /// <summary>
    /// グローバルなデータを格納するクラス
    /// </summary>
    class C_GlobalData
    {
        /// <summary>
        /// SQLのコネクション
        /// </summary>
#if POSTGRESQL
        static NpgsqlConnection m_SqlConnection = null;
#elif SQLIGHT
        static SQLiteConnection m_SqlConnection = null;
#endif

        /// <summary>
        /// SQLのコネクションのオープン・リトライオープン
        /// </summary>
        static public void sqlOpen()
        {
            try
            {
                
#if POSTGRESQL
                m_SqlConnection = new NpgsqlConnection(C_PostgreSettings.ConnectText);
#elif SQLIGHT
                if (File.Exists(C_PostgreSettings.SQLightDBPath) == false) throw new Exception("ファイルなし\r\n" + C_PostgreSettings.SQLightDBPath);
                m_SqlConnection = new SQLiteConnection(C_PostgreSettings.ConnectText);
#endif          
                m_SqlConnection.Open();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("データベース接続失敗\r\n\r\n" + ex.Message,
                                                     "エラー",
                                                     System.Windows.Forms.MessageBoxButtons.OK,
                                                     System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// SQLのコネクションのクローズ
        /// </summary>
        static public void sqlClose()
        {
            try
            {
                // SQL接続のクローズ 
                if (m_SqlConnection != null)
                {
                    m_SqlConnection.Close();
                    m_SqlConnection = null;
                }
            }
            catch (Exception)
            {
            }
        }


        /// <summary>
        /// SQLコマンドの送受信 （読み込み）
        /// </summary>
        /// <param name="command">SQLのコマンド</param>
        static public DbDataAdapter sqlRead2DataAdapter(string command, params object[] args)
        {            
            if (args.Length > 0) command = string.Format(command, args);
#if POSTGRESQL
            return new NpgsqlDataAdapter(command, m_SqlConnection);
#elif SQLIGHT
            return new SQLiteDataAdapter(command, m_SqlConnection);
#endif
        }


        public static DbCommand sqlCommand(string commandText)
        {
#if POSTGRESQL
            return new NpgsqlCommand(commandText, m_SqlConnection);
#elif SQLIGHT
            return new SQLiteCommand(commandText, m_SqlConnection);
#endif
        }

        /// <summary>
        /// SQLコマンドの送受信 （読み込み）
        /// </summary>
        /// <param name="command">SQLのコマンド</param>
        static public DbDataReader sqlRead(string command, params object[] args)
        {
            try
            {
                if (args != null)
                {
                    if (args.Length > 0) command = string.Format(command, args);
                }
                
                using (var cmd = sqlCommand(command))
                {
                    return cmd.ExecuteReader();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// SQLコマンドの送信 （書き込み）
        /// </summary>
        /// <param name="command">SQLのコマンド</param>
        static public void sqlWrite(string command, params object[] args)
        {
            try
            {
                if (args != null)
                {
                    if (args.Length > 0) command = string.Format(command, args);
                }

                using (var cmd = sqlCommand(command))
                {
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
            }
        }


    
        /// <summary>
        /// DBのテーブルから所定の列データを取り出す
        /// </summary>
        /// <param name="tableName">テーブル名 （例：t_検索システム）</param>
        /// <param name="readCol">取り出したい列の名前 （例：col_画像ファイル名）</param>
        /// <param name="where">where 文 (例：where col_電算管理番号 = '0000100001')</param>
        /// <returns>取り出したデータ  (例：0000100001.gif)</returns>
        static public object sqlReadDataFromTable(string tableName, string readCol, string where)
        {
            try
            {
                using (var reader = sqlRead("SELECT " + readCol + " FROM " + tableName + " " + where))
                {
                    reader.Read();
                    object outData = reader[readCol];
                    reader.Close();
                    if (outData.GetType().Name.ToUpper().CompareTo("DBNULL") == 0) outData = null;
                    return outData;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// テーブルから件数をカウントする
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="rowName">列の名前</param>
        /// <param name="data">SQLのデータ</param>
        /// <returns></returns>
        static public int sqlCount(string tableName, string rowName, string data)
        {
            try
            {
                using (var reader = sqlRead("SELECT COUNT(" + rowName + ") FROM " + tableName + " WHERE " + rowName + "=" + data))
                {
                    reader.Read();
                    return Convert.ToInt32(reader[0]);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }
        
        /// <summary>
        /// DBのテーブルに行を追加する。キーが重複する場合は上書きする
        /// </summary>
        /// <param name="tableName">テーブル名 （例：t_検索システム）</param>
        /// <param name="colList">列の名前の一覧 （例：col_電算管理番号, col_ファイル名, col_ファイル件数, ･･･） ※ 先頭は主キー</param>
        /// <param name="valueList">出力データ一覧 （例：'電算0001', 'aaa.gif', 4, ･･･）</param>
        static public void sqlInsert(string tableName, string[] colList, string[] valueList)
        {
            try
            {
                string outcommand;

                if (sqlCount(tableName, colList[0] ,valueList[0]) <= 0) // 重複する行数が０ ⇒ insertコマンドで追加する
                {
                    outcommand = string.Format("INSERT INTO {0} ({1}) VALUES ({2})",
                                               tableName,
                                               string.Join(" , ", colList),
                                               string.Join(" , ", valueList)
                                              );
                    //outcommand = "insert into " + tableName + " ( ";
                    //for (int i = 0; i < colList.Length; i++)
                    //{
                    //    if (i > 0) outcommand += " , ";
                    //    outcommand += colList[i];
                    //}
                    //outcommand += " ) values ( ";
                    //for (int i = 0; i < valueList.Length; i++)
                    //{
                    //    if (i > 0) outcommand += " , ";
                    //    outcommand += valueList[i];
                    //}
                    //outcommand += " )";
                }
                else
                {                               // 重複する行あり　⇒ updateコマンドで上書きする
                    string[] tmp = new string[colList.Length - 1];
                    for (int i = 0; i < tmp.Length; i++) tmp[i] = string.Format("{0} = {1}", colList[i + 1], valueList[i + 1]);
                    outcommand = string.Format("UPDATE {0} SET {1} WHERE {2} = {3}",
                                               tableName,
                                               string.Join(" , ", tmp),
                                               colList[0],
                                               valueList[0]
                                               );
                    //outcommand = "update " + tableName + " set ";
                    //for (int i = 1; i < colList.Length; i++)
                    //{
                    //    if (i > 1) outcommand += " , ";
                    //    outcommand += colList[i] + " = " + valueList[i];
                    //}
                    //outcommand += " where " + colList[0] + " = " + valueList[0];
                }

                sqlWrite(outcommand);
            }
            catch (Exception)
            {
            }
        }

        
        public enum e_dbType
        {
            Text,
            Integer,
            Bigint,
            Timestamp,
            Date,
        }


        public static void AddDBParam(DbCommand cmd, string paramname, e_dbType type, object value)
        {
            AddDBParam(cmd, paramname, type);
            cmd.Parameters[paramname].Value = value;
        }

        public static void AddDBParam(DbCommand cmd, string paramname, e_dbType type)
        {
#if POSTGRESQL
            var c = (NpgsqlCommand)cmd;
            switch (type)
            {
                case e_dbType.Text:
                    c.Parameters.Add(paramname, NpgsqlTypes.NpgsqlDbType.Text);
                    break;
                case e_dbType.Integer:
                    c.Parameters.Add(paramname, NpgsqlTypes.NpgsqlDbType.Integer);
                    break;
                case e_dbType.Bigint:
                    c.Parameters.Add(paramname, NpgsqlTypes.NpgsqlDbType.Bigint);
                    break;
                case e_dbType.Timestamp:
                    c.Parameters.Add(paramname, NpgsqlTypes.NpgsqlDbType.Timestamp);
                    break;
                case e_dbType.Date:
                    c.Parameters.Add(paramname, NpgsqlTypes.NpgsqlDbType.Date);
                    break;
                default:
                    break;
            }
#elif SQLIGHT
            var c = (SQLiteCommand)cmd;
            switch (type)
            {
                case e_dbType.Text:
                    c.Parameters.Add(paramname, DbType.String);
                    break;
                case e_dbType.Integer:
                    c.Parameters.Add(paramname, DbType.Int32);
                    break;
                case e_dbType.Bigint:
                    c.Parameters.Add(paramname, DbType.Int64);
                    break;
                case e_dbType.Timestamp:
                    c.Parameters.Add(paramname, DbType.DateTime);
                    break;
                case e_dbType.Date:
                    c.Parameters.Add(paramname, DbType.DateTime);
                    break;
                default:
                    break;
            }
#endif
        }

        /// <summary> トランザクションの開始 </summary>
        /// <returns></returns>
        static public DbTransaction sqlBeginTransaction()
        {            
            return m_SqlConnection.BeginTransaction();
        }

        /// <summary>
        /// 自分自身のアプリのパスを取得する
        /// </summary>
        static public string getAppDir()
        {
            string path = System.Reflection.Assembly.GetEntryAssembly().Location;
            return Path.GetDirectoryName(path);
        }





        static private C_SettingData readXML()
        {
            C_SettingData value = null;
            FileStream fs = null;
            try
            {
                // XmlSerializer オブジェクトの生成
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(C_SettingData));

                // Xmlファイルオープン (読み込みモード)
                String xmlFileName = getAppDir() + @"\検索システム.xml";      //　XMLファイル名
                fs = new FileStream(xmlFileName, FileMode.Open);

                // 逆シリアル化する
                value = (C_SettingData)serializer.Deserialize(fs);
            }
            catch (Exception)
            {
            }

            try
            {
                fs.Close();
            }
            catch (Exception)
            {
            }
            
            if (value == null)    value = new C_SettingData();      // 逆シリアル化失敗 ⇒ デフォルト値を返す･･･

            return value;
        }

        static private void writeXML(C_SettingData arg)
        {
            FileStream fs = null;
            try
            {
                // XmlSerializer オブジェクトの生成
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(C_SettingData));

                // Xmlファイルオープン (書き込みモード)
                String xmlFileName = getAppDir() + @"\検索システム.xml";      //　XMLファイル名
                fs = new FileStream(xmlFileName, FileMode.Create);

                // シリアル化する
                serializer.Serialize(fs, arg);
            }
            catch (Exception e)
            {
            }

            try
            {
                fs.Close();
            }
            catch (Exception)
            {
            }
        }


        /// <summary>
        /// 読み込み元ドライブ名
        /// </summary>
        static public string driveName
        {
            get
            {
                return readXML().driveName;
            }
            
            set
            {
                C_SettingData dat = readXML();
                dat.driveName = value;
                writeXML(dat);
            }
        }

        /// <summary>
        /// 印刷用プリンタの設定を取得する
        /// </summary>
        /// <param name="printerName">プリンタ名</param>
        /// <param name="printMargin">余白（ミリ）</param>
        static public void getPrinterSetting(out string printerName, out int printMargin)
        {
            C_SettingData dat = readXML();
            printerName = dat.pinterName;
            printMargin = dat.printMargin;
        }

        /// <summary>
        /// 印刷用プリンタの設定を変更する
        /// </summary>
        /// <param name="printerName">プリンタ名</param>
        /// <param name="printMargin">余白（ミリ）</param>
        static public void writePrinterSetting(string printerName, int printMargin)
        {
            C_SettingData dat = readXML();
            dat.pinterName = printerName;
            dat.printMargin = printMargin;
            writeXML(dat);
        }


        


        /// <summary>
        /// string -> int (Int32) の変換 
        /// </summary>
        /// <param name="s">string値</param>
        /// <returns>int値 （例外時は0を返す）</returns>
        static public uint string_2_uint32(string s)
        {
            /** string -> int (Int32) の変換 （例外時は0を返す） */
            try
            {
                return UInt32.Parse(s);
            }
            catch (Exception)
            {
                return 0;
            }
             
        }

        static JapaneseCalendar m_JapaneseCalendar = new JapaneseCalendar();

        /// <summary>
        /// 日付 → 和暦5桁の変換
        /// </summary>
        /// <param name="arg">日付</param>
        /// <returns>和暦5桁（"GYYMM" Gは和暦コード） (エラー時は空白)</returns>
        static public string date_2_wareki5(DateTime arg)
        {
            try
            {   
                return m_JapaneseCalendar.GetEra(arg).ToString("0") +      // 西暦コード
                       m_JapaneseCalendar.GetYear(arg).ToString("00") +    // 年
                       m_JapaneseCalendar.GetMonth(arg).ToString("00");    // 月
            }
            catch (Exception)
            {
                return "";
            }
        }


        /// <summary>
        /// 性別コード ⇒ 性別（DB用）への変換
        /// </summary>
        /// <param name="code">性別コード"1" or "2"</param>
        /// <returns>性別"'男'" or "'女'"</returns>
        public static string sql_sexCode_2_sex(string code)
        {
            try
            {
               switch (int.Parse(code))
               {
                   case 1:
                       return   "'男'";
                   case 2:
                       return   "'女'";
                   default:
                       return "'null'";
               }
            }
            catch (Exception)
            {
                return "'null'";
            }
        }

        /// <summary>
        /// 和暦 ⇒ 日付 (DB用) への変換
        /// </summary>
        /// <param name="wareki">和暦、"gYYMMDD"の７文字、 g は元号(0が明治～4が平成)、 YYMMDDは年月日 </param>
        /// <returns>DBに出力させる日付 ('YYYY/MM/DD') </returns>
        public static string sql_wareki_2_date(string wareki)
        {
            try
            {                
                int e = int.Parse(wareki.Substring(0,1));       // 元号                   1:明治 2:大正 3:昭和 4:平成 
                int y = int.Parse(wareki.Substring(1,2));       // 年                
                string m = wareki.Substring(3,2);               // 月
                string d = wareki.Substring(5, 2);              // 日
                
                // 西暦への変換
                y = WarekiModule.wareki_2_seireki(e, y);
                return "'" + y.ToString("0000") + "/" + m + "/" + d + "'";

            }
            catch (Exception)
            {
                return "null";
            }
        }


        

        /// <summary>
        /// 年月（YYYYMM） ⇒ 日付 (DB用) への変換
        /// </summary>
        /// <param name="ym">年月（診療年月・請求年月）"YYYYMM"の6文字 </param>
        /// <returns>DBに出力させる1日の日付 ('YYYY/MM/DD') </returns>
        public static string sql_ym_2_date(string ym)
        {
            try
            {
                return "'" + ym.Substring(0, 4) + "/" + ym.Substring(4, 2) + "/01'";
                    
            }
            catch (Exception)
            {
                return "null";
            }
        }


        /// <summary>
        /// 給付区分コード ⇒ 給付区分 への変換
        /// </summary>
        /// <param name="typeCode">給付区分コード（"7" or "8" or "9"）</param>
        /// <returns>給付区分("7:鍼灸" or "8:あんま" or "9:柔道整復" )</returns>
        public static string sql_typeCode_2_type(string typeCode)
        {
            try
            {
                switch (int.Parse(typeCode))
                {
                    case 7:
                        return "'7:鍼灸'";
                    case 8:
                        return "'8:あんま'";
                    case 9:
                        return "'9:柔道整復'";
                    default:
                        return "'" + typeCode + "'";                // 入力した給付区分コードをそのまま出力する！
                }
            }
            catch (Exception)
            {
                return "'" + typeCode + "'";                // 入力した給付区分コードをそのまま出力する！
            }
        }


        /// <summary> 読み込みログ（t_readlog）　の出力
        /// <param name="dataid"></param>
        /// <param name="discno"></param>
        /// <param name="disccount"></param>
        /// <param name="colinfo"></param>
        /// <returns>col_logno （シリアル値）</returns>
        public static long WriteToReadlog(string dataid, int discno, int disccount, string colinfo = "")
        {
            try
            {
                using (var cmd = sqlCommand("INSERT INTO t_ReadLog (col_readtime,  col_dataid,  col_discno,  col_disccount,  col_pcname,  col_info) VALUES " +
                                            "(:col_readtime, :col_dataid, :col_discno, :col_disccount, :col_pcname, :col_info) RETURNING col_logno"))
                {
                    AddDBParam(cmd, "col_readtime", e_dbType.Timestamp);
                    AddDBParam(cmd, "col_dataid", e_dbType.Text);
                    AddDBParam(cmd, "col_discno", e_dbType.Integer);
                    AddDBParam(cmd, "col_disccount", e_dbType.Integer);
                    AddDBParam(cmd, "col_pcname", e_dbType.Text);
                    AddDBParam(cmd, "col_info", e_dbType.Text);

                    cmd.Parameters["col_readtime"].Value = DateTime.Now;
                    cmd.Parameters["col_dataid"].Value = dataid;
                    cmd.Parameters["col_discno"].Value = discno;
                    cmd.Parameters["col_disccount"].Value = disccount;
                    cmd.Parameters["col_pcname"].Value = System.Net.Dns.GetHostName();
                    cmd.Parameters["col_info"].Value = colinfo;

                    using (var r = cmd.ExecuteReader())
                    {
                        r.Read();
                        return Convert.ToInt64(r[0]);
                    }
                }

            }
            catch (Exception)
            {
                return 0L;
            }
        }


        /// <summary> 読み込みログ（t_readlog）　の出力
        /// <param name="logno"></param>
        /// <param name="colinfo"></param>
        /// <returns>col_logno （シリアル値）</returns>
        public static void WriteToReadlog(long logno, string colinfo = "")
        {
            try
            {
                using (var cmd = sqlCommand("UPDATE t_ReadLog SET col_info = :col_info WHERE col_logno = :col_logno"))
                {
                    AddDBParam( cmd, "col_logno", e_dbType.Bigint);
                    AddDBParam(cmd, "col_info", e_dbType.Text);
                    cmd.Parameters["col_logno"].Value = logno;
                    cmd.Parameters["col_info"].Value = colinfo;
                    cmd.ExecuteNonQuery();
                }

            }
            catch (Exception)
            {
            }

        }

        /// <summary> 被保険者番号の補正 （所定の長さに固定する。）</summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static string GetInsuredNo(string arg)
        {
            arg = arg.Trim();
#if KADOMA_CITY
            int len = int.Parse(ReceSeek_Harikyu.Properties.Resources.KADOMA_INSUREDNO_LEN);     // 門真市、被保険者番号の文字列長 （固定）
            if (arg.Length > len) arg = arg.Substring(arg.Length - len);        // 後ろの６文字を取得。それ以前をカット。
            else if (arg.Length < len) arg = arg.PadLeft(len, '0');            // ゼロ埋めする
            return arg;
#else
            return arg;
#endif
        }


        /// <summary> 本紙＋続紙の一覧を取得する </summary>
        /// <param name="fname"></param>
        /// <returns>本紙＋続紙の一覧（フルパス名）</returns>
        static public List<string> GetZokushiList(object fname)
        {
            var list = new List<string>();

            int next = -1;
            using (var cmd = C_GlobalData.sqlCommand("SELECT fname FROM t_data WHERE fname > :fname AND medimonth > 0 AND medimonth < 9999 ORDER BY fname LIMIT 1"))
            {
                AddDBParam(cmd, "fname", e_dbType.Text);
                cmd.Parameters["fname"].Value = fname;

                using (var r = cmd.ExecuteReader())
                {
                    if (r.Read()) next = Convert.ToInt32(r[0]);
                }
            }



            string query = "SELECT * FROM t_data WHERE fname = :fname UNION " +
                           "SELECT * FROM t_data WHERE fname > :fname AND medimonth = 9999";
            if (next > 0) query += " AND fname < :next";
            query += " ORDER BY fname";



            // 本紙＋続紙検索のコマンドを発行
            using (var cmd = C_GlobalData.sqlCommand(query))
            {
                C_GlobalData.AddDBParam(cmd, "fname", C_GlobalData.e_dbType.Text);
                C_GlobalData.AddDBParam(cmd, "next", C_GlobalData.e_dbType.Text);
                cmd.Parameters["fname"].Value = fname;
                cmd.Parameters["next"].Value = next;
                
                using (var r = cmd.ExecuteReader())
                {
                    while (r.Read())
                    {
                        string n = r["fname"].ToString().PadLeft(9, '0');       // ファイル名（９桁固定）
                        list.Add(C_PostgreSettings.PictureRootPath + @"\img\" + n.Substring(0, 3) + @"\" + n.Substring(0, 6) + @"\" + n + ".gif");
                    }
                }
            }

            return list;
        }


    }



}
